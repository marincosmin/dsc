#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "slist.h"
#include "hashtable.h"

extern const hash_fn hashfns[];

struct pair {
    void            *key;
    void            *value;
    struct fn_table *fntbl;
};

static inline int
_compare_pair(const void* key, const void* data)
{
    struct pair *pair = (struct pair *)data;

    return pair->fntbl->compare(key, pair->key);
}

static inline void
_deinit_pair(void *data, void *fextra)
{
    struct pair *pair = (struct pair *)data;

    if (NULL != pair->fntbl->deinit)
        pair->fntbl->deinit(NULL, pair->value);
    pair->value = NULL;

    free(pair->key);
    pair->key = NULL;

    free(pair);
}

static inline void
_walk_pair(void* key, void *data, void* extra)
{
    struct pair *pair = (struct pair *)data;

    (void)key;

    pair->fntbl->walk(pair->key, pair->value, extra);
}

static inline void
_print_pair(const void *data, const void *fextra)
{
    struct pair *pair = (struct pair *)data;

    printf(">> ");
    pair->fntbl->print(pair->key, pair->value);
    printf("\n");
}

int
hashtable_init(struct hashtable** hashtable, unsigned seed,
               unsigned capacity, hash_fn hash, walk_fn walk,
               print_fn print, deinit_fn deinit, compare_fn compare)
{
    struct hashtable *h;

    if (NULL == hashtable)
        return -EINVAL;

    h = calloc(1, sizeof(struct hashtable));
    if (NULL == h)
        return -ENOMEM;

    h->fntbl = calloc(1, sizeof(struct fn_table));
    if (NULL == h->fntbl) {
        free(h);
        return -ENOMEM;
    }
    
    h->capacity = (0 == capacity) ? DEFAULT_CAPACITY : capacity;
    h->buckets = calloc(hashsize(h->capacity), sizeof(struct slist *));
    if (NULL == h->buckets) {
        free(h->fntbl);
        free(h);
        return -ENOMEM;
    }

    for (unsigned i = 0; i < hashsize(h->capacity); i++) {
        int rc = slist_init(&h->buckets[i], _walk_pair,
                            _print_pair, _deinit_pair, _compare_pair);
        if (rc) {
            for (unsigned j = 0; j < i; j++)
                slist_deinit(h->buckets[j]);
            free(h->buckets);
            free(h->fntbl);
            free(h);
            return rc;
        }
    }

    h->seed = seed;
    if (0 == seed) {
        srandom((unsigned) (0xFFFFFFFF & (uintptr_t)hashtable)); 
        h->seed = (unsigned)random(); 
    }

    h->hash = (NULL == hash) ? hashfns[HASH_JENKINS] : hash;
    h->fntbl->walk = walk;
    h->fntbl->print = print;
    h->fntbl->deinit = deinit;
    h->fntbl->compare = compare;

    *hashtable = h;

    return EOK;
}

int
hashtable_insert(struct hashtable* hashtable,
                 void* key, uint32_t size, void* data)
{
    uint32_t hashkey = 0;
    struct pair *pair = NULL;
    struct slist *bucket = NULL;

    if (NULL == hashtable || NULL == key || NULL == data)
        return -EINVAL;

    hashkey = hashtable->hash((const uint8_t *)key,
                              size, (uint32_t *)&hashtable->seed);
    bucket = hashtable->buckets[hashkey & hashmask(hashtable->capacity)];
 
    /* check whether key exists & update value in case found*/
    pair = (struct pair *)slist_find(bucket, key);
    if (NULL != pair) {
        pair->value = data;
        return EOK;
    }

    /* create a new {key, value} pair */
    pair = calloc(1, sizeof(struct pair));
    if (NULL == pair)
        return -ENOMEM;
   
    /* copy the key in the pair */
    pair->key = calloc(size, sizeof(char));
    if (NULL == pair->key) {
        free(pair);
        return -ENOMEM;
    }
    memcpy(pair->key, key, size);

    pair->value = data;
    pair->fntbl = hashtable->fntbl;

    slist_add_front(bucket, pair);

    return EOK;
}

void*
hashtable_search(struct hashtable* hashtable, void* key, uint32_t size)
{
    uint32_t hashkey = 0;
    struct pair* pair = NULL;
    struct slist *bucket = NULL;

    if (NULL == hashtable || NULL == key || 0 == size)
        return NULL;

    hashkey = hashtable->hash((const uint8_t *)key,
                              size, (uint32_t *)&hashtable->seed);
    bucket = hashtable->buckets[hashkey & hashmask(hashtable->capacity)];
    pair = slist_find(bucket, key);

    //TODO: what if it's a list of elements ?
    if (NULL == pair)
        return NULL;
    return pair->value;
}

int
hashtable_remove(struct hashtable* hashtable, void* key, uint32_t size)
{
    uint32_t hashkey = 0;
    struct slist *bucket = NULL;

    if (NULL == hashtable || NULL == key || 0 == size)
        return -EINVAL;

    hashkey = hashtable->hash((const uint8_t *)key,
                       size, (uint32_t *)&hashtable->seed);
    bucket = hashtable->buckets[hashkey & hashmask(hashtable->capacity)];
    
    slist_del(bucket, key);

    return EOK;
}

int
hashtable_walk(struct hashtable* hashtable, void* extra, walk_fn walk)
{
    walk_fn wfn;

    if (NULL == hashtable)
        return -EINVAL;

    wfn = (NULL == walk) ? hashtable->fntbl->walk : walk;

    for (unsigned i = 0; i < hashsize(hashtable->capacity); i++) {
        if (slist_empty(hashtable->buckets[i]))
            continue;
        slist_walk(hashtable->buckets[i], extra, wfn);
    }

    return EOK;
}

int
hashtable_print(struct hashtable *hashtable)
{
    if (NULL == hashtable)
        return -EINVAL;

    for (unsigned i = 0; i < hashsize(hashtable->capacity); i++) {
        if (slist_empty(hashtable->buckets[i]))
            continue;
        printf("list: %d\n", i);
        slist_print(hashtable->buckets[i]);
    }

    return EOK;
}

int
hashtable_deinit(struct hashtable* hashtable)
{
    if (NULL == hashtable)
        return -EINVAL;

    for (unsigned i = 0; i < hashsize(hashtable->capacity); i++) {
        slist_deinit(hashtable->buckets[i]);
        hashtable->buckets[i] = NULL;
    }

    if (hashtable->buckets) {
        free(hashtable->buckets);
        hashtable->buckets = NULL;
    }

    if (hashtable->fntbl) {
        free(hashtable->fntbl);
        hashtable->fntbl = NULL;
    }

    free(hashtable);

    return EOK;
}
