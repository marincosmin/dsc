#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hashtable.h"

static int _strcmp(const void *a, const void *b)
{
    char *sa = (char *)a;
    char *sb = (char *)b;
    
    return strcmp(sa, sb);
}

static void _strprint(const void *key, const void *value)
{
    char *k = (char *)key;
    int *v = (int *)value;

    printf("%s:%d", k, *v);
}

int main(int argc, char *args[])
{
    struct hashtable *h;
    char *keys[] = {"maine", "texas", "alabama", "ohio",
                      "montana", "california", "newmexico"};
    int occurrences[] = {103, 10, 21, 3, 43, 15, 6};

    hashtable_init(&h, 0, 3, NULL, NULL, _strprint, NULL, _strcmp);

    for (int i = 0; i < sizeof(occurrences)/sizeof(int); i++) {
        hashtable_insert(h, keys[i], strlen(keys[i]) + 1, &occurrences[i]); 
    }
    hashtable_print(h);
    
    printf("remove: %s\n", keys[5]);
    hashtable_remove(h, keys[5], strlen(keys[5]) + 1);
    hashtable_print(h);
    
    printf("remove: %s\n", keys[3]);
    hashtable_remove(h, keys[3], strlen(keys[3]) + 1);
    hashtable_print(h);
   
    printf("update: %s\n", keys[1]);
    hashtable_insert(h, keys[1], strlen(keys[1]) + 1, &occurrences[3]); 
    hashtable_print(h);

    hashtable_deinit(h);

    return 0;
}
