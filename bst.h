#ifndef _BST_H
#define _BST_H

#include "common.h"

struct key {
    void            *key;
    unsigned        length;
};

struct pair {
    struct key      key;
    void            *value;
    unsigned        refcnt;
};

struct bst_node {
    struct pair     *payload;
    struct bst_node *lchild;
    struct bst_node *rchild;
};

struct bst {
    struct bst_node *root;
    struct fn_table fntbl;
};

int
bst_init(struct bst **bst, walk_fn walk, print_fn print,
         deinit_fn deinit, compare_fn compare);

int
bst_deinit(struct bst *bst);

int
bst_insert(struct bst *bst, void *key, unsigned length, void *data);

void *
bst_search(struct bst *bst, void *key);

void *
bst_get_max(struct bst *bst);

void *
bst_get_min(struct bst *bst);

int
bst_delete(struct bst *bst, void *key);

int
bst_walk(struct bst *bst, walk_fn walk);

int
bst_print(struct bst *bst);
#endif /* _BST_H */
