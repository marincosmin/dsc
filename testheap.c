#include <stdio.h>
#include <stdlib.h>

#include "heap.h"

int intcmp(const void *a, const void *b)
{
    int *ia = (int *)a;
    int *ib = (int *)b;
    
    if (*ia == *ib)
        return 0;
    if (*ia > *ib)
        return 1;
    return -1;
}

void intprint(const void *a, const void *b)
{
    int *ia = (int *)a;

    printf("%d ", *ia);
}

int main(int argc, char *args[])
{
    struct heap *h; 
    int a = 7;
    int b = 1;
    int c = 4;
    int d = 2;
    int e = 9;
    int f = 5;
    int g = 10;
    int i = 3;
    int j = 6;

    if (heap_init(&h, MINHEAP, NULL, intprint, NULL, intcmp))
        printf("init failed");

    heap_insert(h, &a);

    heap_insert(h, &b);
    
    heap_insert(h, &c);

    heap_insert(h, &d);
    
    heap_insert(h, &e);
    
    heap_insert(h, &f);
    
    heap_insert(h, &g);
    
    heap_insert(h, &i);
    
    heap_insert(h, &j);

    heap_print(h);
    printf("\n");

    printf("top = %d\n", *(int *)heap_peek(h));
    printf("top = %d\n", *(int *)heap_extract(h));
    heap_print(h);
    printf("\n");

    printf("top = %d\n", *(int *)heap_peek(h));    
    heap_print(h);
    printf("\n");

    printf("top = %d\n", *(int *)heap_extract(h));    
    heap_print(h);
    printf("\n");

    heap_insert(h, &i);
    heap_print(h);
    printf("\n");
    
    heap_insert(h, &d);
    heap_print(h);
    printf("\n");
    
    heap_insert(h, &b);
    heap_print(h);
    printf("\n");

    heap_deinit(h);

    free(h);

    return 0;
}
