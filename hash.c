#include "hash.h"

const hash_fn hashfns[] = {
    [HASH_JENKINS] = jenkins_hash,
    [HASH_SUPERFAST] = super_fast_hash
};

