#ifndef HASHTABLE_H_
#define HASHTABLE_H_

#include "slist.h"
#include "hash.h"

#define hashsize(n) ((uint32_t)1 << (n))
#define hashmask(n) (hashsize(n) - 1)

#define DEFAULT_CAPACITY        10

struct hashtable {
    unsigned        capacity;   // the power of 2
    unsigned        seed;       // hash table's hash function's seed
    hash_fn         hash;       // hashing function
    struct fn_table *fntbl;     // function table
    struct slist    **buckets;  // hash buckets
};

/**
 * @brief Initialises a hashtable.
 *
 * @param hashtable
 *   Pointer to a hashtable pointer.
 * @param seed
 *   Hash function seed.
 * @param capacity
 *   Hashtable capacity.
 * @param walk
 *   Pointer to a function dumping the content of the node
 * @param deinit
 *   Pointer to a function providing hashtable deinitialization.
 * @param compare
 *   Pointer to a comparison function that provides order relation between 
 *   list nodes; can be NULL in case list usage does not imply nodes
 *   comparison.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid pointer to the list.
 * @retval -ENOMEM for memory allocation failure.
 * @retval EOK for success.
 */
int
hashtable_init(struct hashtable** hashtable, unsigned seed,
               unsigned capacity, hash_fn hash, walk_fn walk,
               print_fn print, deinit_fn deinit, compare_fn compare);

/**
 * @brief Uninitialises the hashtable.
 *
 * @param hashtable
 *   Pointer to the hashtable.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the hashtable.
 * @retval EOK in case of success.
 */
int
hashtable_deinit(struct hashtable* hashtable);

/**
 * @brief Insert a [key,value] pair to the hash.
 *
 * @param hashtable
 *   Pointer to the hashtable.
 * @param key
 * @param size
 * @param data
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the hashtable.
 * @retval EOK in case of success.
 */
int
hashtable_insert(struct hashtable *hashtable,
                 void *key, uint32_t size, void *data);

/**
 * @brief Looks up for the existence of a [key, value] pair in the hashtable.
 *
 * @param hashtable
 *   Pointer to the hashtable.
 * @param key
 * @param size
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the hashtable.
 * @retval EOK in case of success.
 */
void *
hashtable_search(struct hashtable *hashtable, void *key, uint32_t size);

/**
 * @brief Removes a [key, value] pair in the hashtable.
 *
 * @param hashtable
 *   Pointer to the hashtable.
 * @param key
 * @param size
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the hashtable.
 * @retval EOK in case of success.
 */
int
hashtable_remove(struct hashtable *hashtable, void *key, uint32_t size);

/**
 * @brief Walks the hashtable and calls a function on each entry.
 *
 * @param hashtable
 *   Pointer to the hashtable.
 * @param extra
 * @param walk
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the hashtable.
 * @retval EOK in case of success.
 */
int 
hashtable_walk(struct hashtable *hashtable, void *extra, walk_fn walk);

int
hashtable_print(struct hashtable *hashtable);
#endif
