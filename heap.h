#ifndef _HEAP_H
#define _HEAP_H

#include "common.h"
#include "vector.h"

enum heap_type {
    MINHEAP = 1,
    MAXHEAP
};

struct heap {
    enum heap_type  type;
    struct vector   vector;
};

/**
 * @brief Initialises a heap.
 *
 * @param heap
 *   Pointer to the heap.
 * @param type
 *   Heap type: MINHEAP / MAXHEAP.
 * @param walk
 * @param print
 *   Pointer to a function dumping the content of a heap node.
 * @param deinit
 *   Pointer to a function handling deinitialisation of a heap node; can be
 *   NULL in case node deinitialisation isn't required.
 * @param compare
 *   Pointer to a comparison function that provides order relation between 
 *   heap nodes; can be NULL in case list usage does not imply nodes.
 *   comparison.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid pointer to the heap.
 * @retval -ENOMEM in case heap memory allocation fails.
 * @retval EOK in case of success.
 */
int
heap_init(struct heap **heap, const enum heap_type type, 
          walk_fn walk, print_fn print, deinit_fn deinit, compare_fn compare);

/**
 * @brief Uninitialises the heap.
 *
 * @param heap
 *   Pointer to the heap.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the heap.
 * @retval EOK in case of success.
 */
int
heap_deinit(struct heap *heap);

/**
 * @brief Inserts a new element in the heap.
 *
 * @param heap
 *   Pointer to the heap.
 * @param data
 *   Pointer to the data of the new element to be inserted..
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the heap.
 * @retval EOK in case of success.
 */
int
heap_insert(struct heap *heap, void *data);

/**
 * @brief Retrieves the value at the root of the heap.
 *
 * @param heap
 *   Pointer to the heap.
 *
 * @return The value at the root of the heap.
 *
 * @retval Pointer to the min/max heap value.
 * @retval NULL in case of invalid parameter or empty heap.
 */
void *
heap_peek(struct heap *heap);

/**
 * @brief Retrieves the value at the root of the heap after removing it..
 *
 * @param heap
 *   Pointer to the heap.
 *
 * @return The value at the root of the heap.
 *
 * @retval Pointer to the min/max heap value.
 * @retval NULL in case of invalid parameter or empty heap.
 */
void *
heap_extract(struct heap *heap);

/**
 * @brief Gets the size of the heap.
 *
 * @param[in]  heap
 *   Pointer to the heap.
 *
 * @return The number of elements in the heap.
 *
 * @retval EINVAL for invalid parameter. 
 * @retval Integer number >= 0.
 */
int
heap_size(struct heap *heap);

/**
 * @brief Checks whether the heap is empty.
 *
 * @param[in]  heap
 *   Pointer to the heap.
 *
 * @return
 *
 * @retval 1 (True).
 * @retval 0 (False).
 */
int
heap_empty(struct heap *heap);

/**
 * @brief Prints the content of the heap.
 *
 * @oaram heap
 *   Pointer to the heap.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the heap or unset print
 *         function.
 * @retval EOK in case of success.
 */
int
heap_print(struct heap *heap);
#endif
