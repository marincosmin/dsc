#include <stdlib.h>
#include <string.h>

#include "slist.h"

#define INIT_NODE(node, data)                                               \
    do {                                                                    \
        node = calloc(1, sizeof(struct slist_node));                        \
        if (NULL == list)                                                   \
            return -ENOMEM;                                                 \
        node->data = (data);                                                \
    } while (0)

int
slist_init(struct slist **list, walk_fn walk, print_fn print,
           deinit_fn deinit, compare_fn compare)
{
    struct slist *l = NULL;

    if (NULL == list)
        return -EINVAL;

    l = calloc(1, sizeof(struct slist));
    if (NULL == l)
        return -ENOMEM;

    l->fntbl = calloc(1, sizeof(struct fn_table));
    if (NULL == l->fntbl) {
        free(l);
        return -ENOMEM;
    }

    l->fntbl->compare = compare;
    l->fntbl->deinit = deinit;
    l->fntbl->print = print;
    l->fntbl->walk = walk;

    *list = l;

    return EOK;
}

int
slist_empty(struct slist *list)
{
    if (NULL == list)
        return -EINVAL;

    return (list->size == 0);
}

int
slist_size(struct slist *list)
{
    if (NULL == list) 
        return -EINVAL;

    return list->size;
}

int
slist_add_front(struct slist *list, void *data)
{
    struct slist_node *node = NULL;
    struct slist_node *head = NULL;

    if (NULL == list)
        return -EINVAL;

    INIT_NODE(node, data);

    head = SLIST_HEAD(list);
    node->next = head->next;
    head->next = node;

    list->size++;

    return EOK;
}

int
slist_add_back(struct slist *list, void *data)
{
    struct slist_node *node = NULL;
    struct slist_node *iter = NULL;

    if (NULL == list)
        return -EINVAL;
   
    INIT_NODE(node, data);

    iter = SLIST_HEAD(list);

    while (iter->next != NULL)
        iter = iter->next;

    iter->next = node;

    list->size++;

    return EOK;
}

//TODO: check implementation
int
slist_insert(struct slist *list, void *key, void *data)
{
    struct slist_node *node = NULL;
    struct slist_node *iter = NULL;

    if (NULL == list || NULL == key)
        return -EINVAL;

    if (NULL == list->fntbl->compare)
        return -EUNINIT;

    iter = SLIST_HEAD(list)->next;
    while (iter != NULL) {
        if (!list->fntbl->compare(key, iter->data))
            break;
        iter = iter->next;
    }
   
    if (NULL == iter)
        return -ENOTFND;

    INIT_NODE(node, data);
   
    node->next = iter->next;
    iter->next = node;

    list->size++;

    return EOK;
}

int
slist_del(struct slist *list, void *key)
{
    struct slist_node *iter = NULL;
    struct slist_node *prev = NULL;

    if (NULL == list)
        return -EINVAL;

    if (NULL == list->fntbl->compare)
        return -EUNSET;

    if (!slist_empty(list)) {
        prev = SLIST_HEAD(list);
        iter = prev->next;

        while (iter != NULL) {
            if (!list->fntbl->compare(key, iter->data))
                break;
            prev = iter;
            iter = iter->next;
        }

        if (NULL == iter)
            return -ENOTFND;

        prev->next = iter->next;

        if (list->fntbl->deinit)
            list->fntbl->deinit(iter->data, NULL);
        free(iter);

        list->size--;
    }

    return EOK;
}

int
slist_del_front(struct slist *list)
{
    struct slist_node *head = NULL;
    struct slist_node *victim = NULL;

    if (NULL == list)
        return -EINVAL;

    if (!slist_empty(list)) {
        head = SLIST_HEAD(list);
        victim = head->next;
        head->next = victim->next;

        if (list->fntbl->deinit)
            list->fntbl->deinit(victim->data, NULL);
        free(victim);

        list->size--;
    }

    return EOK;
}

int
slist_del_back(struct slist *list)
{
    struct slist_node *prev = NULL;
    struct slist_node *iter = NULL;

    if (NULL == list)
        return -EINVAL;

    if (!slist_empty(list)) {
        prev = SLIST_HEAD(list);
        iter = prev->next;
        while (iter->next != NULL) {
            prev = iter;
            iter = iter->next;
        }

        prev->next = NULL;

        if (list->fntbl->deinit)
            list->fntbl->deinit(iter->data, NULL);
        free(iter);

        list->size--;
    }

    return EOK;
}

void *
slist_find(struct slist *list, void *key)
{
    struct slist_node *iter = NULL;

    if (NULL == list || NULL == key)
        return NULL;
    
    if (NULL == list->fntbl->compare)
        return NULL;

    iter = SLIST_HEAD(list)->next;
    while (iter != NULL) {
        if (!list->fntbl->compare(key, iter->data))
            break;
        iter = iter->next;
    }

    if (NULL == iter)
        return NULL;
    
    return iter->data;
}

//TODO: implement merge sort
int
slist_sort(struct slist *list)
{
    return 0;
}

int
slist_walk(struct slist *list, void *extra, walk_fn walk)
{
    walk_fn wfn;
    unsigned index = 0;
    struct slist_node *iter = NULL;

    if (NULL == list)
        return -EINVAL;

    // provided walk function takes precedence over the previously set one
    wfn = (NULL == walk) ? list->fntbl->walk : walk;
    if (NULL == wfn)
        return -EUNSET;

    iter = SLIST_HEAD(list)->next;
    while (iter != NULL) {
        wfn(iter->data, &index, extra);
        index++;
    }

    return EOK;
}

int
slist_print(struct slist *list)
{
    unsigned index = 0;
    struct slist_node *iter = NULL;

    if (NULL == list)
        return -EINVAL;

    //TODO: find a better error code
    if (NULL == list->fntbl->print)
        return -EUNSET;

    iter = SLIST_HEAD(list)->next;
    while (iter != NULL) {
        list->fntbl->print(iter->data, &index);
        iter = iter->next;
        index++;
    }

    return EOK;
}

int
slist_deinit(struct slist *list)
{
    struct slist_node *iter = NULL;

    if (NULL == list)
        return -EINVAL;
    
    iter = SLIST_HEAD(list)->next;
    while (iter != NULL) {
        struct slist_node *tmp = iter->next;
        if (list->fntbl->deinit)
            list->fntbl->deinit(iter->data, NULL);
        free(iter);
        iter = tmp;
    }

    free(list->fntbl);
    free(list);

    return EOK;
}
