#include <stdio.h>

#include "stack.h"

void intprint(const void *a, const void *p)
{
    int *ia = (int *)a;

    printf("%d ", *ia);
}


int main(int argc, char *args[])
{
    int array[] = {1, 2, 3, 4, 5, 6};

    STACK(stack, intprint, NULL);

    for (int i = 0; i < sizeof(array) / sizeof(array[0]); i++)
       stack_push(&stack, &array[i]);

    stack_print(&stack);
    printf("\n");

    printf("top = %d\n", *(int *)stack_top(&stack));
    stack_pop(&stack);
    
    printf("top = %d\n", *(int *)stack_top(&stack));
    stack_pop(&stack);
    
    stack_print(&stack);
    printf("\n");

    stack_push(&stack, &array[sizeof(array)/sizeof(array[0]) - 1]);
    stack_print(&stack);
    printf("\n");

    stack_deinit(&stack);

    return 0;
}
