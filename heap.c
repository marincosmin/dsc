#include <string.h>
#include <stdlib.h>

#include "heap.h"

#define BUBBLE_ON   1
#define BUBBLE_OFF  0

int 
heap_init(struct heap **heap, enum heap_type type, 
          walk_fn walk, print_fn print, deinit_fn deinit, compare_fn compare)
{
    int rc = 0;
    struct heap *h = NULL;

    if (heap == NULL || compare == NULL)
        return -EINVAL;

    if (type < MINHEAP || type > MAXHEAP)
        return -EINVAL;

    h = calloc(1, sizeof(struct heap));
    if (NULL == h)
        return -ENOMEM;

    rc = vector_init(&h->vector, 0, walk, print, deinit, compare);
    if (rc)
        goto _end;

    h->type = type;

    *heap = h;

_end:
    return rc;
}

int
heap_deinit(struct heap *heap)
{
    int rc = 0;

    if (heap == NULL)
        return -EINVAL;
    
    rc = vector_deinit(&heap->vector);
    if (rc)
        goto _end;

    memset(heap, 0, sizeof(struct heap));
    free(heap);

_end:
    return rc;
}

int
heap_size(struct heap *heap)
{
    if (heap == NULL)
        return -EINVAL;

    return vector_size(&heap->vector);
}

// -EINVAL heap = NULL
int
heap_empty(struct heap *heap)
{
    if (heap == NULL)
        return -EINVAL;

    return (0 == vector_size(&heap->vector));
}

static inline int
_bubble(struct heap *heap, unsigned int current, unsigned int parent)
{
    int result = 0;

    result = vector_compare(&heap->vector, current, parent);

    switch (heap->type) {
        case MINHEAP:
            if (result == LT)
                return BUBBLE_ON;
            return BUBBLE_OFF; 
        case MAXHEAP:
            if (result == GT)
                return BUBBLE_ON;
            return BUBBLE_OFF;

        default:
            return BUBBLE_OFF;
    }
}

//assumes both indices are valid
static unsigned int
_get_candidate(struct heap *heap, unsigned int left, unsigned int right)
{
    int result;

    result = vector_compare(&heap->vector, left, right);

    switch (heap->type) {
        case MINHEAP:
            if (result < 0)
                return left;
            return right;

        case MAXHEAP:
            if (result < 0)
                return right;
            return left;

        // should never reach this point
        default:
            return left;
    }
}

static void
_bubble_down(struct heap *heap)
{
    unsigned int parent = 0;
    unsigned int lchild = 1;
    unsigned int rchild = 2;
    unsigned int candidate = 0;
    
    int last = vector_size(&heap->vector);

    while (lchild < last) {
        candidate = (rchild < last) ?
                _get_candidate(heap, lchild, rchild) : lchild;

        candidate = _get_candidate(heap, parent, candidate);

        if (candidate == parent)
            break;

        vector_swap(&heap->vector, candidate, parent);
        
        parent = candidate;
        lchild = (parent << 1) + 1;
        rchild = (parent << 1) + 2;
    }    
}

static void
_bubble_up(struct heap *heap)
{
    unsigned int parent = 0;
    unsigned int current = 0;

    current = vector_size(&heap->vector) - 1;
    parent = current >> 1;

    while ((current != 0) && (BUBBLE_ON == _bubble(heap, current, parent))) {
        vector_swap(&(heap->vector), current, parent);

        current = parent;
        parent = current >> 1; 
    }
}

int
heap_insert(struct heap *heap, void *node)
{
    int rc = 0;

    if (heap == NULL)
        return -EINVAL;

    rc = vector_add(&heap->vector, node);
    if (rc)
        goto _end;

    _bubble_up(heap);    

_end:
    return rc;
}

void *
heap_peek(struct heap *heap)
{
    if (heap == NULL)
        return NULL;

    if (0 == vector_size(&heap->vector))
        return NULL;

    return vector_get(&heap->vector, 0);
}

void *
heap_extract(struct heap *heap)
{
    int last = 0;
    void *node = NULL;

    if (heap == NULL)
        return NULL;

    //TODO: why get isn't returning NULL when empty ?
    node = vector_get(&heap->vector, 0);
    if (NULL == node)
        return NULL;

    last = vector_size(&heap->vector) - 1;
    vector_swap(&heap->vector, 0, last);
    vector_del(&heap->vector, last);

    _bubble_down(heap);

    return node;
}

int
heap_print(struct heap *heap)
{
    if (heap == NULL)
        return -EINVAL;

    return vector_print(&heap->vector);
}
