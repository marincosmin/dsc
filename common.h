#ifndef _COMMON_H
#define _COMMON_H

#include <errno.h>
#include <stddef.h>

#define EOK     0
#define EUNSET  200
#define EUNINIT 201
#define EIDXOOB 202
#define ENOTFND 202

#define TRUE    1
#define FALSE   0

#define LT          -1
#define EQ          0
#define GT          1

struct meta {
    unsigned entries;
    unsigned entrysize;
    unsigned capacity;
};

#define INC_STATS(meta)     ((meta)->entries)++
#define DEC_STATS(meta)     ((meta)->entries)--

/*
 * Determine the natural order of two keys.
 */
typedef int (*compare_fn)(const void* key1, const void* key2);

/*
 * Pretty printing of data stored in the DS.
 */
typedef void (*print_fn)(const void* data, const void* fextra);

/*
 * Uninitialise data stored as an entry in the DS.
 */
typedef void (*deinit_fn)(void* data, void* fextra);

/*
 * Execute walk function on every entry of the DS.
 *
 * @param data
 * @param fextra - extra data passed by framework; 
 *                  the key - in case of a hashtable
 *                  the index - in case of an array
 *                  TODO: life span of this object ?
 * @param uextra - extra data passed by the user
 */
typedef void (*walk_fn)(void* data, void* fextra, void* uextra);

struct fn_table {
    walk_fn    walk;
    print_fn   print;
    deinit_fn  deinit;
    compare_fn compare;
};

typedef int (*comparefn)(const void *, const void *);

typedef void (*freefn)(const void*);

typedef void (*printfn)(const void*);

// ptr to node, node index, data to be passed
typedef void (*foreachfn)(void *, unsigned, void *);

typedef void (*walkfn)(void *, unsigned, void *);

struct fntbl {
    comparefn cmp;
    freefn  free;
    printfn print;
    walkfn walk;
};
#endif /*_COMMON_H*/
