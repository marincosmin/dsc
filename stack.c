#include <stdlib.h>

#include "stack.h"

int
stack_init(struct stack **stack, print_fn print, deinit_fn deinit)
{
    struct stack *s = NULL;

    if (NULL == stack)
        return -EINVAL;

    s = calloc(1, sizeof(struct stack));
    if (NULL == s)
        return -ENOMEM;

    s->fntbl.print = print;
    s->fntbl.deinit = deinit;

    return EOK;
}

int
stack_empty(struct stack *stack)
{
    struct stack_node *top = NULL;

    if (NULL == stack)
        return -EINVAL;

    top = STACK_TOP(stack);
    return (NULL == top->next);
}

int
stack_size(struct stack *stack)
{
    if (NULL == stack)
        return -EINVAL;

    return stack->size;
}

static inline struct stack_node*
_init_stack_node(void *data)
{
    struct stack_node *node = NULL;

    node = calloc(1, sizeof(struct stack_node));
    if (NULL == node)
        return NULL;

    node->data = data;

    return node;
}

int
stack_push(struct stack *stack, void *data)
{
    struct stack_node *top = NULL;
    struct stack_node *node = NULL;

    if (NULL == stack)
        return -EINVAL;

    node = _init_stack_node(data);
    if (NULL == node)
        return -ENOMEM;

    top = STACK_TOP(stack);
    node->next = top->next;
    top->next = node;

    stack->size++;

    return 0;
}

void *
stack_top(struct stack *stack)
{
    struct stack_node *top = NULL;

    if (NULL == stack)
        return NULL;

    top = STACK_TOP(stack);
    if (NULL == top->next)
        return NULL;
    return top->next->data;
}

void *
stack_pop(struct stack *stack)
{
    void *data = NULL;
    struct stack_node *top = NULL;
    struct stack_node *victim = NULL;

    if (NULL == stack)
        return NULL;

    top = STACK_TOP(stack);
    victim = top->next;
    if (NULL == victim)
        return NULL;
    
    //unlink node
    top->next = victim->next;

    data = victim->data;
    free(victim);

    stack->size--;

    return data;
}

int
stack_deinit(struct stack *stack)
{
    struct stack_node *top = NULL;
    struct stack_node *iter = NULL;

    if (NULL == stack)
        return -EINVAL;

    top = STACK_TOP(stack);
    iter = top->next;
    while (iter != NULL) {
        struct stack_node *tmp = iter->next;
        if (stack->fntbl.deinit)
            stack->fntbl.deinit(iter->data, NULL);
        free(iter);
        iter = tmp;
    }

    return EOK;
}

int
stack_print(struct stack *stack)
{
    struct stack_node *top = NULL;
    struct stack_node *iter = NULL;

    if (NULL == stack)
        return -EINVAL;

    //TODO: find a better error code
    if (NULL == stack->fntbl.print)
        return -EUNSET;

    top = STACK_TOP(stack);
    iter = top->next;
    while (iter != NULL) {
        stack->fntbl.print(iter->data, NULL);
        iter = iter->next; 
    }

    return EOK;
}
