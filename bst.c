#include <stdlib.h>
#include <string.h>

#include "bst.h"

int
bst_init(struct bst **bst, walk_fn walk, print_fn print,
         deinit_fn deinit, compare_fn compare)
{
    struct bst *t;

    t = calloc(1, sizeof(struct bst));
    if (NULL == t)
        return -ENOMEM;

    t->fntbl.walk = walk;
    t->fntbl.print = print;
    t->fntbl.deinit = deinit;
    t->fntbl.compare = compare;

    *bst = t;

    return EOK;
}

static void
_do_deinit(struct bst_node *root, deinit_fn deinit)
{
    if (NULL == root)
        return;

    _do_deinit(root->lchild, deinit);
    _do_deinit(root->rchild, deinit);

    if (deinit)
        deinit(root->payload->value, NULL);

    free(root->payload->key.key);
    free(root->payload);
    free(root);
}

int
bst_deinit(struct bst *bst)
{
    if (NULL == bst)
        return -EINVAL;
    
    _do_deinit(bst->root, bst->fntbl.deinit);

    free(bst);

    return EOK;
}

#define INIT_BST_NODE(node, key, length, data)                              \
    do {                                                                    \
        node = calloc(1, sizeof(struct bst_node));                          \
        if (NULL == node)                                                   \
            return -ENOMEM;                                                 \
        node->payload = calloc(1, sizeof(struct pair));                     \
        if (NULL == node->payload) {                                        \
            free(node);                                                     \
            return -ENOMEM;                                                 \
        }                                                                   \
        node->payload->key.length = length;                                 \
        node->payload->key.key = calloc(length, sizeof(char));              \
        if (NULL == node->payload->key.key) {                               \
            free(node->payload);                                            \
            free(node);                                                     \
        }                                                                   \
        node->payload->refcnt++;                                            \
        memcpy(node->payload->key.key, key, length);                        \
        node->payload->value = data;                                        \
    } while (0)

static void
_do_insert(struct bst_node *root, struct bst_node *node, compare_fn compare)
{
    //TODO: do we want direct access ?
    if (compare(root->payload->key.key, node->payload->key.key) >= 0) {
        if (NULL == root->lchild) {
            root->lchild = node;
        } else {
            _do_insert(root->lchild, node, compare);
        }
    } else {
        if (NULL == root->rchild) {
            root->rchild = node;
        } else {
            _do_insert(root->rchild, node, compare);
        }
    }
}

int
bst_insert(struct bst *bst, void *key, unsigned length, void *data)
{
    struct bst_node *node;

    if (NULL == bst || NULL == key)
        return -EINVAL;
    if (0 == length)
        return -EINVAL;

    INIT_BST_NODE(node, key, length, data);

    if (NULL == bst->root) {
        bst->root = node;
        return EOK;
    }
   
    _do_insert(bst->root, node, bst->fntbl.compare);

    return EOK;
}

static void *
_do_search(struct bst_node *root, void *key, compare_fn compare)
{
    if (NULL == root)
        return NULL;

    switch(compare(root->payload->key.key, key)) {
        case 0:
            return root->payload->value;
        case 1:
            return _do_search(root->lchild, key, compare);
        case -1:
            return _do_search(root->rchild, key, compare);
        default:
            return NULL;
    }
}

void *
bst_search(struct bst *bst, void *key)
{
    if (NULL == bst || NULL == key)
        return NULL;

    if (NULL == bst->fntbl.compare)
        return NULL;

    return _do_search(bst->root, key, bst->fntbl.compare);
}

static void *
_do_get_max(struct bst_node *current)
{
    if (NULL == current)
        return NULL;
    if (NULL == current->rchild)
        return current;
    return _do_get_max(current->rchild);
}

static void *
_do_get_predecessor(struct bst_node *current)
{
    if (NULL == current)
        return NULL;
    return _do_get_max(current->lchild);
}

//in case of empty tree return NULL
void *
bst_get_max(struct bst *bst)
{
    struct bst_node *node;

    if (NULL == bst)
        return NULL;
    node = _do_get_max(bst->root);
    if (NULL == node)
        return NULL;
    return node->payload->value;
}

static void *
_do_get_min(struct bst_node *current)
{
    if (NULL == current)
        return NULL;
    if (NULL == current->lchild)
        return current;
    return _do_get_min(current->lchild);
}

static void *
_do_get_successor(struct bst_node *current)
{
    if (NULL == current)
        return NULL;
    return _do_get_min(current->rchild);
}


void *
bst_get_min(struct bst *bst)
{
    struct bst_node *node;

    if (NULL == bst)
        return NULL;
    node = _do_get_min(bst->root);
    if (NULL == node)
        return NULL;
    return node->payload->value;
}

static void *
_do_delete(struct bst_node *current, void *key, compare_fn compare)
{
    int cmp;
    struct bst_node *node;

    if (NULL == current)
        return NULL;

    cmp = compare(current->payload->key.key, key);

    if (cmp > 0) {
        current->lchild = _do_delete(current->lchild, key, compare);
    } else if (cmp < 0) {
        current->rchild = _do_delete(current->rchild, key, compare);
    } else {
        if (NULL == current->lchild) {
            node = current->rchild;
            current->payload->refcnt--;
            if (!current->payload->refcnt) {
                free(current->payload->key.key);
                free(current->payload);
            }
            free(current);
            return node;
        } else if (NULL == current->rchild) {
            node = current->lchild;
            current->payload->refcnt--;
            if (!current->payload->refcnt) {
                free(current->payload->key.key);
                free(current->payload);
            }
            free(current);
            return node;
        } else {
            node = _do_get_predecessor(current);

            free(current->payload->key.key);
            free(current->payload);
            
            node->payload->refcnt++;
            current->payload = node->payload;
            current->lchild = _do_delete(current->lchild,
                                         node->payload->key.key, compare);
        }
    }

    return current;
}

int
bst_delete(struct bst *bst, void *key)
{
    if (NULL == bst || NULL == key)
        return -EINVAL;

    _do_delete(bst->root, key, bst->fntbl.compare);

    return EOK;
}

int
bst_walk(struct bst *bst, walk_fn walk)
{
    if (NULL == bst)
        return -EINVAL;

    return EOK;
}

static void
_do_print(struct bst_node *root, print_fn print)
{
    if (NULL == root)
        return;

    _do_print(root->lchild, print);
    if (print)
        print(root->payload->key.key, root->payload->value);
    _do_print(root->rchild, print);
}

int
bst_print(struct bst *bst)
{
    if (NULL == bst)
        return -EINVAL;
   
    _do_print(bst->root, bst->fntbl.print);

    return EOK;
}
