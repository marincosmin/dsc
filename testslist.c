#include <stdio.h>

#include "slist.h"

int intcmp(const void *a, const void *b)
{
    int *ia = (int *)a;
    int *ib = (int *)b;
    
    if (*ia == *ib)
        return 0;
    if (*ia > *ib)
        return 1;
    return -1;
}

void intprint(const void *a, const void *extra)
{
    int *ia = (int *)a;

    printf("%d ", *ia);
}

int main(int argc, char *args[])
{
    int x = 9;
    int array[] = {1, 2, 3, 4, 5, 6};
    struct slist *list;

    slist_init(&list, NULL, intprint, NULL, intcmp);

    for (int i = 0; i < sizeof(array) / sizeof(array[0]); i++) {
        if (i % 2)
            slist_add_front(list, &array[i]);
        else
            slist_add_back(list, &array[i]);
    }
    
    slist_print(list);
    printf("\n");

    slist_del(list, &array[3]);

    slist_print(list);
    printf("\n");

    slist_insert(list, &array[2], &x);
    
    slist_print(list);
    printf("\n");

    slist_del_front(list);

    slist_print(list);
    printf("\n");

    slist_del_back(list);

    slist_print(list);
    printf("\n");

    slist_deinit(list);

    return 0;
}
