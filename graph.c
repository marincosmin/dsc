#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>

#include "graph.h"

extern int errno;

static void
free_edge(void* edge, void *dummy)
{
    struct edge *e = (struct edge *)edge;

    if (NULL != e)
        free(e);
}

static void
print_edge(const void* edge, const void *dummy)
{
    struct edge *e = (struct edge *)edge;

    if (NULL != e)
        printf("  N:%d->W:%d\n", e->neighbour->label, e->weight);
}

static inline int
_graph_init(struct graph *graph, int num_nodes, int *labels, int num_edges)
{
    int i = 0;

    graph->nodes = calloc(num_nodes, sizeof(struct graph_node));
    if (NULL == graph->nodes)
        return -ENOMEM;

    for (i = 0; i < num_nodes; i++) {
        graph->nodes[i].label = (NULL == labels) ? i : labels[i];
        graph->nodes[i].color = WHITE;
        graph->nodes[i].distance = MAX_DISTANCE;
        graph->nodes[i].previous = NULL;
    }

    graph->num_nodes = num_nodes;
    graph->num_edges = num_edges;

    return EOK;
}

static inline int
_add_edge(struct graph *graph, unsigned current, unsigned endpoint, int weight)
{
    struct graph_node *node = NULL;
    struct edge *edge = NULL;
    
    node = &graph->nodes[current];
    if (NULL == node->edges) {
        slist_init(
            &node->edges, NULL, print_edge, free_edge, NULL);
    }

    edge = calloc(1, sizeof(struct edge));
    if (NULL == edge) {
        //TODO: free memory & report error
        return -ENOMEM;
    }

    edge->weight = weight;
    edge->neighbour = &graph->nodes[endpoint];
    slist_add_front(node->edges, edge);

    return EOK;
}

/*
 * 0-based
 */
int
graph_initl(struct graph **graph, int type, unsigned num_nodes,
            int *labels, unsigned num_edges, ...)
{
    int rc;
    va_list args;
    struct graph *g;

    if (NULL == graph) {
        rc = -EINVAL;
        goto _end;
    }

    g = calloc(1, sizeof(struct graph));
    if (NULL == g) {
        rc = -ENOMEM;
        goto _end;
    }

    va_start(args, num_edges);

    rc = _graph_init(g, num_nodes, labels, num_edges);
    if (rc)
        goto _end;

    for (int i = 0; i < num_edges; i++) {
                
        int x = va_arg(args, int);
        int y = va_arg(args, int);
        int w = (type & WEIGHTED) ? va_arg(args, int) : 0;

        if (x > num_nodes || y > num_nodes) {
            //TODO: free memory & report error
            goto _end;
        }

        rc = _add_edge(g, x, y, w);
#ifdef DEBUG
        printf("adding: %d -> %d w=%d %s\n",
               x, y, weight, (EOK == rc) ? "ok" : "fail");
#endif
        if (rc)
            goto _end;
    }

    *graph = g;

    rc = EOK;

_end:
    va_end(args);
    return rc;
}

int
graph_initv(struct graph **graph, int flags, unsigned num_nodes,
            int *labels, unsigned num_edges, int *edges, int *weights)
{
    int rc;
    struct graph *g;

    if (NULL == graph)
        return -EINVAL;

    g = calloc(1, sizeof(struct graph));
    if (NULL == g)
        return -ENOMEM;

    rc = _graph_init(g, num_nodes, labels, num_edges);
    if (rc)
        goto _end;

    for (int i = 0; i < num_edges; i++) {
                
        int x = edges[2 * i];
        int y = edges[2 * i + 1];
        int w = (NULL != weights) ? weights[i] : 1;

        // 0-based node's label indexing
        if (x >= num_nodes || y >= num_nodes) {
            //TODO: free memory & report error
            goto _end;
        }
        
        rc = _add_edge(g, x, y, w);
        //TODO: check return value and handle error
        if (!IS_DIRECTED_GRAPH(flags))
            rc = _add_edge(g, y, x, w);
#ifdef DEBUG
        printf("adding: %d -> %d w=%d %s\n",
               x, y, weight, (EOK == rc) ? "ok" : "fail");
#endif
        if (rc)
            goto _end;
    }

    *graph = g;

    rc = EOK;

_end:
    return rc;
}

int
graph_initf(char *file, struct graph **graph, int flags)
{
    int          rc;
    size_t       size;
    unsigned     num_nodes;
    unsigned     num_edges;
    char         *buffer = NULL;
    FILE         *stream = NULL;
    struct graph *g = NULL;

    if (NULL == file || NULL == graph)
        return -EINVAL;

    stream = fopen(file, "r");
    if (NULL == stream)
       return errno;

    // get the number of nodes
    if (getline(&buffer, &size, stream) < 0) {
        //TODO: invalid formatting of file data ?
        rc = errno;
        goto _end;
    }
    sscanf(buffer, "%u", &num_nodes);

    // get the number of edges
    if (getline(&buffer, &size, stream) < 0) {
        //TODO: invalid formatting of file data ?
        rc = errno;
        goto _end;
    }
    sscanf(buffer, "%u", &num_edges);

    // initialize graph
    g = calloc(1, sizeof(struct graph));
    if (NULL == g) {
        rc = -ENOMEM;
        goto _end;
    }
    //TODO: labels are ignored at the moment
    rc = _graph_init(g, num_nodes, NULL, num_edges);
    if (rc) //TODO: uninitialise the graph
        goto _end;

    // get edges
    for (unsigned i = 0; i < num_edges; i++) {
        int weight = 1;
        unsigned src, dst;

        if (getline(&buffer, &size, stream) < 0) {
            rc = errno;
            goto _end;
        }
        if (IS_WEIGHTED_GRAPH(flags))
            sscanf(buffer, "%u %u %d", &src, &dst, &weight);
        else
            sscanf(buffer, "%u %u", &src, &dst);

        if (src >= num_nodes || dst >= num_nodes) {
            //TODO: handle error case
            rc = -EINVAL;
            goto _end;
        }

        rc = _add_edge(g, src, dst, weight);
        if (!IS_DIRECTED_GRAPH(flags))
            rc = _add_edge(g, dst, src, weight);
    }

    *graph = g;

    rc = EOK;

_end:
    if (NULL != stream)
        fclose(stream);
    if (NULL != buffer)
        free(buffer);
    return rc;
}

static int
_dfs(struct graph_node *node)
{
    struct edge *edge = NULL;
    struct graph_node *neighbour = NULL;

    node->color = GRAY;

    for_each_edge(node, edge) {
        neighbour = edge->neighbour;

        if (GRAY == neighbour->color)
            return TRUE;

        if (WHITE == neighbour->color) {
            if (TRUE == _dfs(neighbour))
                return TRUE;
        }
    }

    node->color = BLACK;

    return FALSE;
}

int
graph_has_cycle(struct graph *graph)
{
    struct graph_node *node = NULL;

    for_each_node(graph, node)
        node->color = WHITE;

    for_each_node(graph, node) {
        if (node->color == WHITE)
            if (TRUE == _dfs(node))
                return TRUE;
    }

    return FALSE;
}

/*
 * 0-indexed
 */
struct graph_node *
graph_get_node(struct graph *graph, int label)
{
    if (NULL == graph)
        return NULL;

    if (label >= graph->num_nodes)
        return NULL;

    return &graph->nodes[label];
}

int
graph_print(struct graph *graph)
{
    struct graph_node *node;

    if (NULL == graph)
        return -EINVAL;

    for_each_node(graph, node) {
        printf("N%d\n", node->label);
        slist_print(node->edges);
    }

    return 0;
}

int
graph_deinit(struct graph *graph)
{
    struct graph_node *node;

    if (NULL == graph)
        return -EINVAL;

    for_each_node(graph, node) {
        if (NULL == node->edges)
            continue;

        slist_deinit(node->edges);
        node->edges = NULL;
    }

    free(graph->nodes);
    graph->nodes = NULL;

    free(graph);

    return 0;
}
