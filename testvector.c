#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vector.h"

int intcmp(const void *a, const void *b)
{
    int *ia = (int *)a;
    int *ib = (int *)b;
    
    if (*ia == *ib)
        return 0;
    if (*ia > *ib)
        return 1;
    return -1;
}

void intprint(const void *a, const void *fextra)
{
    int *ia = (int *)a;

    printf("%d ", *ia);
}

void
multiply(void *elem, void *pos, void *mult)
{
    int *e = (int *)elem;
    int *m = (int *)mult;

    *e *= *m;
    //printf("idx=%u nval=%d\n", pos, *e); 
}

int main(int argc, char *args[])
{
    int i;
    int sarray[] = {9, 7, 4, 2, 5, 1, 3, 8, 6};
    int m = 2;
    int x = 20;
    int y = 11;
    int z = 13;

    struct vector v;

    vector_init(&v, 4, NULL, intprint, NULL, intcmp);

    for (i = 0; i < sizeof(sarray)/sizeof(int); i++)
        vector_add(&v, &sarray[i]);

    vector_print(&v);
    printf("\n");

    //vector_foreach(&v, multiply, &m);
    vector_walk(&v, &m, multiply);
    vector_print(&v);
    printf("\n");

    vector_sort(&v, ASCENDING);
    vector_print(&v);
    printf("\n");

    vector_swap(&v, 0, 5);
    vector_print(&v);
    printf("\n");
    
    vector_insert(&v, 3, &x);
    vector_print(&v);
    printf("\n");
    
    vector_set(&v, 0, &z);
    vector_print(&v);
    printf("\n");

    vector_del(&v, vector_size(&v) - 1);
    vector_print(&v);
    printf("\n");
    
    vector_add(&v, &y);
    vector_print(&v);
    printf("\n");
    
    vector_sort(&v, DESCENDING);
    vector_print(&v);
    printf("\n");

    vector_add(&v, &sarray[0]);
    vector_print(&v);
    printf("\n");

    vector_deinit(&v);

    return 0;
}
