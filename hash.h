#ifndef HASH_H_
#define HASH_H_

#include <stdint.h>

uint32_t
jenkins_hash(const uint8_t *data, uint32_t length, uint32_t *seeds);

uint32_t
super_fast_hash(const uint8_t* data, uint32_t length, uint32_t *seeds);

typedef uint32_t (*hash_fn)(
        const uint8_t *data, uint32_t length, uint32_t *seeds);

enum hashfn_id {
    HASH_JENKINS,
    HASH_SUPERFAST
};

#endif /* HASH_H_ */
