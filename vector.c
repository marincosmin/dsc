#include <stdlib.h>
#include <string.h>

#include "vector.h"

#define SWAP_PTR(xptr, yptr)    \
    do {                        \
        void *aux = NULL;       \
        aux = *(xptr);          \
        *(xptr) = *(yptr);      \
        *(yptr) = aux;          \
    } while (0);


// -EINVAL vector = NULL
// -ENOMEM memory allocation for vector fails
int
vector_init(struct vector *vector, const unsigned int capacity, walk_fn walk,
            print_fn print, deinit_fn deinit, compare_fn compare)
{
    unsigned c = 0;

    if (NULL == vector)
        return -EINVAL;

    c = capacity > DEFAULT_CAPACITY ? capacity : DEFAULT_CAPACITY;
    vector->array = calloc(c, sizeof(void *));
    if (NULL == vector->array)
        return -ENOMEM;

    vector->size = 0;
    vector->capacity = c;

    vector->fntbl.walk = walk;
    vector->fntbl.print = print;
    vector->fntbl.deinit = deinit;
    vector->fntbl.compare = compare;

    return 0;
}

// -EINVAL vector = NULL
int
vector_deinit(struct vector *vector)
{
    int i = 0;

    if (NULL == vector)
        return -EINVAL;

    if (vector->fntbl.deinit)
        for (i = 0; i < vector->size; i++)
            vector->fntbl.deinit(&i, vector->array[i]);

    free(vector->array);
    memset(vector, 0, sizeof(struct vector));
   
    return 0;
}

static inline int
_vector_expand(struct vector *vector)
{
    void *vtmp = NULL;
    unsigned int ctmp = 0;

    ctmp = vector->capacity << 1;
    vtmp = realloc(vector->array, ctmp * sizeof(void*));

    if (NULL == vtmp)
        return -ENOMEM;

    vector->array = vtmp;
    vector->capacity = ctmp;

    return 0;
}

// add an element at the end of the vector
// -EINVAL vector = NULL
// -ENOMEM vector needs resizing but we ran out of memory
// -EIDXOOB index is out of vector bounds
int
vector_add(struct vector *vector, void *data)
{
    if (NULL == vector || NULL == data)
        return -EINVAL;

    // vector must be initialized before adding elements
    if (NULL == vector->array)
        return -EUNINIT;
 
    if ((vector->capacity >> 1) < vector->size) {
        if (_vector_expand(vector)) {
            if (vector->capacity == vector->size)
                return -ENOMEM;
        }
    }
    
    vector->array[vector->size] = data;
    vector->size++;

    return 0;
}

// set an element at a specified position in the vector
// if there is something there is gets overwritten
// data can be null
// index must be less than current number of elements in the array
// -EINVAL vector = NULL
// -EUNINIT vector = not initialized
// -EIDXOOB index is out of vector bounds
int
vector_set(struct vector *vector, unsigned index, void *data)
{
    if (NULL == vector)
        return -EINVAL;

    if (NULL == vector->array)
        return -EUNINIT;

    if (index >= vector->size)
        return -EIDXOOB;

    vector->array[index] = data;

    return 0;
}

// inserts an element at a specified position and shifts right
// all the elements in case needed
// -EINVAL vector = NULL
// -EUNINIT vector = not initialized
// -EIDXOOB index is out of vector bounds
int
vector_insert(struct vector *vector, unsigned index, void *data)
{
    unsigned i = 0;

    if (NULL == vector)
        return -EINVAL;

    if (NULL == vector->array)
        return -EUNINIT;

    if (index >= vector->size)
        return -EIDXOOB;

    if (vector->capacity == vector->size) {
        if (_vector_expand(vector)) {
                return -ENOMEM;
        }
    }

    vector->size++;
    for (i = vector->size; i > index; i--)
        vector->array[i] = vector->array[i - 1];
    vector->array[index] = data;

    return 0;
}

// removes the element at the specified index and shifts left
// all the elements past the index in case needed
// -EINVAL vector = NULL
// -EUNINIT vector = not initialized
// -EIDXOOB index is out of vector bounds
int
vector_del(struct vector *vector, unsigned index)
{
    unsigned i = 0;

    if (NULL == vector)
        return -EINVAL;

    if (NULL == vector->array)
        return -EUNINIT;

    if (index >= vector->size)
        return -EIDXOOB;

    for (i = index; i < vector->size; i++)
        vector->array[i] = vector->array[i + 1];
    vector->size--;
    vector->array[vector->size] = NULL;

    return 0;
}

// NULL if :
// vector is NULL
// vector is uninitialised
// index is out of vector's bounds
void *
vector_get(struct vector *vector, unsigned index)
{
    if (NULL == vector || index >= vector->size)
        return NULL;
    
    if (NULL == vector->array)
        return NULL;

    return vector->array[index];
}

// returns the first position starting from index where data
// data can be null
// -EINVAL vector = NULL
// -EUNINIT vector = not initialized
// -EIDXOOB index is out of vector bounds
int
vector_index(struct vector *vector, unsigned index, void *data)
{
    int i = 0;

    if (NULL == vector)
        return -EINVAL;

    if (NULL == vector->array)
        return -EUNINIT;

    if (index >= vector->size)
        return -EIDXOOB;

    for (i = index; i < vector->size; i++)
        if (vector->array[i] == data)
            return i;

    return -ENOTFND; 
}

// -EINVAL vector = NULL
// size
int
vector_size(struct vector *vector)
{
    if (NULL == vector)
        return -EINVAL;
    
    if (NULL == vector->array)
        return -EUNINIT;

    return vector->size;
}

// -EINVAL vector = NULL
// -EUNINIT vector = not initialized
// -EIDXOOB indices are out of vector bounds
inline int
vector_swap(struct vector *vector, unsigned index1, unsigned index2)
{
    if (NULL == vector)
        return -EINVAL;

    if (NULL == vector->array)
        return -EUNINIT;

    if (index1 >= vector->size || index2 >= vector->size)
        return -EIDXOOB;

    if (index1 != index2)
        SWAP_PTR(&vector->array[index1],
                 &vector->array[index2]);

    return EOK;
}

int
vector_compare(struct vector *vector, unsigned index1, unsigned index2)
{
    if (NULL == vector)
        return -EINVAL;

    if (NULL == vector->array)
        return -EUNINIT;

    if (index1 >= vector->size || index2 >= vector->size)
        return -EIDXOOB;

    return vector->fntbl.compare(vector->array[index1],
                                 vector->array[index2]);
}

int
vector_walk(struct vector *vector, void *data, walk_fn walk)
{
    int i = 0;
    walk_fn wfn = NULL;

    if (NULL == vector)
        return -EINVAL;

    wfn = (NULL == walk) ? vector->fntbl.walk : walk;
    if (NULL == wfn)
        return -EUNSET;

    for (i = 0; i < vector->size; i++)
        wfn(vector->array[i], &i, data);
    
    return EOK;
}

static int
_partition(struct vector *vector, int from, int to, order ord)
{
    int i = from - 1;
    int j = from;

    while (j < to) {
        int result = vector->fntbl.compare(
            vector->array[j], vector->array[to]);
        if ((result == LT && ord == ASCENDING) || 
            (result == GT && ord == DESCENDING)) {
            i++;
            SWAP_PTR(&vector->array[j], &vector->array[i]);
        }
        j++;
    }

    i++;
    SWAP_PTR(&vector->array[i], &vector->array[to]);

    return i;
}

static void
_quicksort(struct vector *vector, int start, int end, order ord)
{
    int p = 0;

    if (start > end)
        return;

    p = _partition(vector, start, end, ord);
    _quicksort(vector, start, p - 1, ord);
    _quicksort(vector, p + 1, end, ord);
}

// -EINVAL vector = NULL
// -EUNINIT vector = not initialized
int
vector_sort(struct vector *vector, order ord)
{
    if (NULL == vector)
        return -EINVAL;

    if (ord != ASCENDING && ord != DESCENDING)
        return -EINVAL;

    if (NULL == vector->array || NULL == vector->fntbl.compare)
        return -EUNINIT;

    _quicksort(vector, 0, vector->size - 1, ord);

    return 0;
}

// -EINVAL vector = NULL
// -EUNINIT vector = not initialized
inline int
vector_print(struct vector *vector)
{
    int i = 0;

    if (NULL == vector)
        return -EINVAL;

    if (NULL == vector->array || NULL == vector->fntbl.compare)
        return -EUNINIT;

    for (i = 0; i < vector->size; i++)
        vector->fntbl.print(vector->array[i], &i);

    return 0;
}
