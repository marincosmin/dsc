#ifndef _STACK_H
#define _STACK_H

#include "common.h"

struct stack_node {
    void              *data;
    struct stack_node *next;
};

struct stack {
    unsigned          size;  /* stack current size */
    struct fn_table   fntbl; /* compare, deinit, print, walk functions */
    struct stack_node top;   /* stack's top sentinel */
};

#define STACK_INIT(_print, _deinit)                                         \
    {                                                                       \
        .size  = 0,                                                         \
        .top   = { NULL, NULL},                                             \
        .fntbl = { NULL, _print, _deinit, NULL }                            \
    }

#define STACK(_stack, _print, _deinit)                                      \
		struct stack _stack = STACK_INIT(_print, _deinit)

#define STACK_TOP(_stack)      (&(_stack)->top)

/**
 * @brief Initialises a stack.
 *
 * @param[in]  #stack
 *   Pointer to the stack.
 * @param print
 *   Pointer to a function dumping the content of the node
 * @param deinit
 *   Pointer to a function handling deinitialisation of a node; can be NULL
 *   in case node deinitialisation isn't required.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid pointer to the list.
 * @retval -ENOMEM in case of out of memory.
 * @retval EOK in case of success.
 */
int
stack_init(struct stack **stack, print_fn print, deinit_fn deinit);

/**
 * @brief Check whether a list is empty.
 *
 * @param[in]  #stack
 *   Pointer to the stack.
 *
 * @return
 *
 * @retval 1 (True).
 * @retval 0 (False).
 */
int
stack_empty(struct stack *stack);

/**
 * @brief Get the size of the stack.
 *
 * @param[in]  #stack
 *   Pointer to the stack.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid parameter.
 * @retval integet number >= 0.
 */
int
stack_size(struct stack *stack);

/**
 * @brief Get the element at the top of the stack.
 *
 * @param[in]  #stack
 *   Pointer to the stack.
 *
 * @return Pointer to the top element.
 *
 * @retval NULL for invalid parameter.
 */
void *
stack_top(struct stack *stack);

/**
 * @brief Pushes an the element on the stack.
 *
 * @param[in]  #stack
 *   Pointer to the stack.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid parameter.
 * @retval -ENOMEM ...
 */
int
stack_push(struct stack *stack, void *data);

/**
 * @brief Pops an the element from the stack.
 *
 * @param[in]  #stack
 *   Pointer to the stack.
 *
 * @return Pointer to the top element.
 *
 * @retval NULL for invalid parameter.
 * @retval -ENOMEM ...
 */
void *
stack_pop(struct stack *stack);

/**
 * @brief Uninitialises the stack.
 *
 * @param stack
 *   Pointer to the stack.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the list.
 * @retval EOK in case of success.
 */
int
stack_deinit(struct stack *stack);

/**
 * @brief Prints the content of the stack.
 *
 * @oaram stack
 *   Pointer to the stack.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the list or NULL pointer
 *         to print function
 * @retval EOK in case of success.
 */
int
stack_print(struct stack *stack);


#endif /* _STACK_H */
