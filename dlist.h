#ifndef DLIST_H_
#define DLIST_H_

#include "common.h"

struct dlist_node {
    void              *data;
    struct dlist_node *prev;
    struct dlist_node *next;
};

struct dlist {
    unsigned          size;  /* list's current size */
    struct fn_table   fntbl; /* compare, deinit, print, walk functions */
    struct dlist_node head;  /* list's head sentinel */
    struct dlist_node tail;  /* list's tail sentinel */
};

#define DLIST_INIT(_list, _walk, _print, _deinit, _compare)                 \
    {                                                                       \
        .size = 0,                                                          \
        .fntbl = { _walk, _print, _deinit, _compare },                      \
        .head = { NULL, NULL, &((_list).tail) },                            \
        .tail = { NULL, &((_list).head), NULL }                             \
    }

#define DLIST(_list, _walk, _print, _deinit, _compare)                      \
    struct dlist _list =                                                    \
        DLIST_INIT(_list, _walk, _print, _deinit, _compare) 

#define DLIST_HEAD(list) (&((list)->head))
#define DLIST_TAIL(list) (&(list)->tail)
#if 0
#define _dlist_for_each(iterator, head) \
		for ( iterator = (head)->next; iterator != NULL; iterator = iterator->next)

#define dlist_for_each(iterator, list) \
    _dlist_for_each(iterator, &((list)->head))

#define _dlist_for_each_safe(iterator, temp, head) \
		for (iterator = (head)->next; \
						iterator != NULL && (temp = iterator->next, 1); \
						iterator = temp)

#define dlist_for_each_safe(iterator, temp, list) \
    _dlist_for_each_safe(iterator, temp, &((list)->head))
#endif

//TODO: not allocating the list allows reuse of the same data structure;
//that can be achieved by using a statically allocated list

/**
 * @brief Initialises a double linked list.
 *
 * @param list
 *   Pointer to the list
 * @param walk
 * @param print
 *   Pointer to a function dumping the content of the node
 * @param deinit
 *   Pointer to a function handling deinitialisation of a node; can be NULL
 *   in case node deinitialisation isn't required.
 * @param compare
 *   Pointer to a comparison function that provides order relation between 
 *   list nodes; can be NULL in case list usage does not imply nodes
 *   comparison.
 *
 * @return The error code of the function.
 *
 * @retval EINVAL for invalid pointer to the list.
 * @retval EOK in case of success.
 */
int dlist_init(struct dlist **list, walk_fn walk, print_fn print,
               deinit_fn deinit, compare_fn compare);

/**
 * @brief Checks whether the list is empty.
 *
 * @param list
 *   Pointer to the list.
 *
 * @return The error code of the function.
 *
 * @retval EINVAL for invalid pointer to the list
 * @retval 1 (TRUE) in case the list is empty.
 * @retval 0 (FALSE) in case the list is not empty.
 */
int
dlist_empty(struct dlist *list);

/**
 * @brief Gets the size of the list.
 *
 * @param list
 *   Pointer to the list.
 *
 * @return The number of nodes contained in the list.
 *
 * @retval EINVAL for invalid pointer to the list
 */
int
dlist_size(struct dlist *list);

/**
 * @brief Adds a node at the front of the list.
 *
 * @param list
 *   Pointer to the list.
 * @param data
 *   Pointer to the data carried by the new node.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid pointer to the list.
 * @retval -ENOMEM if a new node can't be allocated.
 * @retval EOK for success.
 */
int
dlist_add_front(struct dlist *list, void *data);

/**
 * @brief Adds a node at the tail of the list.
 *
 * @param list
 *   Pointer to the list.
 * @param data
 *   Pointer to the data carried by the new node.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid pointer to the list.
 * @retval -ENOMEM if a new node can't be allocated.
 * @retval EOK for success.
 */
int
dlist_add_back(struct dlist *list, void *data);

/**
 * @brief Inserts a new node after the node having the key.
 *
 * @param list
 *   Pointer to the list.
 * @param key
 *   Data of the node to insert a new one after.
 * @param data
 *   Pointer to the data carried by the new node.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid pointer to the list.
 * @retval -ENOTFND no node carrying key could be found.
 * @retval -ENOMEM if a new node can't be allocated.
 * @retval EOK for success.
 */
int
dlist_insert(struct dlist *list, void *key, void *data);

/**
 * @brief Deletes the first node in the list.
 *
 * @param list
 *   Pointer to the list.
 *
 * @return Pointer to the deleted node's data in case of success.
 *
 * @retval NULL in case of invalid pointer to the list or empty list.
 */
void *
dlist_del_front(struct dlist *list);

/**
 * @brief Deletes the last node in the list.
 *
 * @param list
 *   Pointer to the list.
 *
 * @return Pointer to the deleted node's data in case of success.
 *
 * @retval NULL in case of invalid pointer to the list or empty list.
 */
void *
dlist_del_back(struct dlist *list);

/**
 * @brief Deletes the node having the key.
 *
 * @param list
 *   Pointer to the list.
 * @param key
 *   Data of the node to be deleted.
 *
 * @return Pointer to the node's data in case of success.
 *
 * @retval NULL in case of invalid pointer to the list, empty list, no node
 *         holding key found or no comparison function provided.
 */
int
dlist_del(struct dlist *list, void *key);

/**
 * @brief Finds the node in the list carrying key.
 *
 * @param list
 *   Pointer to the list.
 *
 * @return Pointer to the node's data in case of success.
 *
 * @retval NULL in case of invalid pointer to the list, empty list or no node
 *         holding key found.
 */
void *
dlist_find(struct dlist *list, void *key);

/**
 * @brief Prints the content of the list.
 *
 * @oaram list
 *   Pointer to the list.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the list or NULL pointer
 *         to print function
 * @retval EOK in case of success.
 */
int
dlist_print(struct dlist *list);

/**
 * @brief Calls on each list's node a user provided function.
 *
 * @param list
 * @param data
 * @param walk
 *
 * @return The error code of the function.
 *
 * @retval EOK     in case of success.
 * @retval -EINVAL in case of invalid pointer to the list data structure.
 * @retval -EUNSET in case user provided no walk function
 */
int
dlist_walk(struct dlist *list, void *data, walk_fn walk);

/**
 * @brief Uninitialises the list.
 *
 * @param list
 *   Pointer to the list.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the list.
 * @retval EOK in case of success.
 */
int
dlist_deinit(struct dlist *list);

/**
 * @brief Free the list. 
 *
 * @param[in]  #head  The pointer to the <head> of the list. The function 
 * assumes the list is created using SLIST_HEAD(head), hence having <head>
 * as a sentinel (does not carry actual data) node. If <head> is NULL no action
 * is performed.
 * @param[in]  #_free  Pointer to the data type freeing function.
 * 
 * @return Pointer to the victim node or NULL in case of empty list or
 * invalid parameters..
 * 
 * @code
 * // assuming the following definition of nodes' payload (<name>) with <name>
 * // being dynamically allocated the user must provide a function that follows
 * // free_node pattern
 * //
 * // struct demo {
 * //    char *name;
 * //    DLIST_LINK(link);
 * // };
 *
 * void
 * free_node(struct slist_node *node)
 * {
 *      struct demo *entry = NULL;
 *
 *      entry = slist_entry(node, struct demo, link);
 *      free(entry->name);  // free node's payload
 *      free(entry);        // free node
 * }
 * @endcode
 */
#endif /* DLIST_H_ */
