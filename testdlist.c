#include <stdio.h>
#include <stdlib.h>

#include "dlist.h"

int intcmp(const void *a, const void *b)
{
    int *ia = (int *)a;
    int *ib = (int *)b;
    
    if (*ia == *ib)
        return 0;
    if (*ia > *ib)
        return 1;
    return -1;
}

void intprint(const void *a, const void *extra)
{
    int *ia = (int *)a;

    printf("%d ", *ia);
}

int main(int argc, char *args[])
{
    int array[] = {1, 2, 3, 4, 5, 6};
    struct dlist *list = NULL;

    dlist_init(&list, NULL, intprint, NULL, intcmp);

    printf("list-empty: %s\n", dlist_empty(list) ? "true" : "false");

    for (int i = 0; i < sizeof(array) / sizeof(array[0]); i++)
        if (i % 2)
            dlist_add_front(list, &array[i]);
        else
            dlist_add_back(list, &array[i]);

    dlist_print(list);
    printf("\n");

    printf("list-size: %d\n", dlist_size(list));

    printf("head: %d\n", *(int *)dlist_del_front(list));
    dlist_print(list);
    printf("\n");
    
    printf("tail: %d\n", *(int *)dlist_del_back(list));
    dlist_print(list);
    printf("\n");

    dlist_insert(list, &array[2], &array[5]);
    dlist_print(list);
    printf("\n");
    
    dlist_del(list, &array[2]);
    dlist_print(list);
    printf("\n");

    printf("found: %d\n", *(int *)dlist_find(list, &array[1]));
    dlist_print(list);
    printf("\n");

    dlist_deinit(list);
    free(list);

    return 0;
}
