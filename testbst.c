#include <stdio.h>

#include "bst.h"

int intcmp(const void *a, const void *b)
{
    int *ia = (int *)a;
    int *ib = (int *)b;
    
    if (*ia == *ib)
        return 0;
    if (*ia > *ib)
        return 1;
    return -1;
}

void intprint(const void *a, const void *extra)
{
    int *ia = (int *)a;
    char *ce = (char *)extra;

    printf("%d: %s ", *ia, ce);
}

int main(int argc, char *args[])
{
    struct bst *bst = NULL;
    char *alphabet[] = {"alpha", "beta", "gamma", "teta",  "lambda", "ro",
                        "fi",    "niu",  "miu",   "omega", "delta",  "pi"};
    int occurrences[] = {99, 10, 21, 3, 43, 15, 104, 6, 51, 103, 13, 109};
    int unknown = 56;

    bst_init(&bst, NULL, intprint, NULL, intcmp);

    printf("initalised\n");
    for (int i = 0; i < sizeof(occurrences)/sizeof(occurrences[0]); i++)
        bst_insert(bst, &occurrences[i], sizeof(occurrences[0]), alphabet[i]);
   
    bst_print(bst);
    printf("\n");

    printf("key:%d, value:%s\n", occurrences[2],
            (char *) bst_search(bst, &occurrences[2]));
    printf("key:%d, value:%s\n", unknown,
            (char *) bst_search(bst, &unknown));
    printf("min value:%s\n", (char *) bst_get_min(bst));
    printf("max value:%s\n", (char *) bst_get_max(bst));

    bst_delete(bst, &occurrences[0]);
    bst_print(bst);
    printf("\n");

    bst_deinit(bst);

    return 0;
}
