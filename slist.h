#ifndef _SLIST_H
#define _SLIST_H

#include "common.h"

struct slist_node {
    void              *data;
    struct slist_node *next;
};

struct slist {
    unsigned          size;   /* list's current size */
    struct slist_node head;   /* list's head sentinel */
    struct fn_table   *fntbl; /* compare, deinit, print, walk functions */
};
#if 0
#define SLIST_INIT(_list, _walk, _print, _deinit, _compare)                 \
    {                                                                       \
        .size  = 0,                                                         \
        .head  = { NULL, NULL},                                             \
        .fntbl = { _walk, _print, _deinit, _compare }                       \
    }

#define SLIST(_list, _walk, _print, _deinit, _compare)                      \
    struct slist _list =                                                    \
        SLIST_INIT(_list, _walk, _print, _deinit, _compare)
#endif
#define SLIST_HEAD(list)    (&((list)->head))

#define slist_for_each(_list, _type, _data)                                 \
    for (struct slist_node *_iter = SLIST_HEAD(_list)->next;                \
        _iter != NULL && (_data = (_type *)_iter->data, 1);                 \
        _iter = _iter->next)

/**
 * @brief Initialises a single linked list.
 *
 * @param list
 *   Pointer to the list
 * @param walk
 * @param print
 *   Pointer to a function dumping the content of the node
 * @param deinit
 *   Pointer to a function handling deinitialisation of a node; can be NULL
 *   in case node deinitialisation isn't required.
 * @param compare
 *   Pointer to a comparison function that provides order relation between 
 *   list nodes; can be NULL in case list usage does not imply nodes
 *   comparison.
 *
 * @return The error code of the function.
 *
 * @retval EINVAL for invalid pointer to the list, EOK otherwise
 */
int
slist_init(struct slist **list, walk_fn walk, print_fn print,
           deinit_fn deinit, compare_fn compare);

/**
 * @brief Checks whether the list is empty.
 *
 * @param list
 *   Pointer to the list.
 *
 * @return The error code of the function.
 *
 * @retval EINVAL for invalid pointer to the list
 * @retval 1 (TRUE) in case the list is empty.
 * @retval 0 (FALSE) in case the list is not empty.
 */
int
slist_empty(struct slist *list);

/**
 * @brief Gets the size of the list.
 *
 * @param list
 *   Pointer to the list.
 *
 * @return The number of nodes contained in the list.
 *
 * @retval EINVAL for invalid pointer to the list
 */
int
slist_size(struct slist *list);

/**
 * @brief Adds a node at the front of the list.
 *
 * @param list
 *   Pointer to the list.
 * @param data
 *   Pointer to the data carried by the new node.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid pointer to the list.
 * @retval -ENOMEM if a new node can't be allocated.
 * @retval EOK for success.
 *
 * O(1)
 */
int
slist_add_front(struct slist *list, void *data);

/**
 * @brief Adds a node at the back of the list.
 *
 * @param list
 *   Pointer to the list.
 * @param data
 *   Pointer to the data carried by the new node.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid pointer to the list.
 * @retval -ENOMEM if a new node can't be allocated.
 * @retval EOK for success.
 *
 * O(n)
 */
int
slist_add_back(struct slist *list, void *data);

/**
 * @brief Inserts a new node after the node having the key.
 *
 * @param list
 *   Pointer to the list.
 * @param key
 *   Data of the node to insert a new one after.
 * @param data
 *   Pointer to the data carried by the new node.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid pointer to the list.
 * @retval -ENOTFND no node carrying key could be found.
 * @retval -ENOMEM if a new node can't be allocated.
 * @retval EOK for success.
 */
int
slist_insert(struct slist *list, void *key, void *data);

/**
 * @brief Deletes the node having the key.
 *
 * @param list
 *   Pointer to the list.
 * @param key
 *   Data of the node to be deleted.
 *
 * @return Pointer to the node's data in case of success.
 *
 * @retval NULL in case of invalid pointer to the list, empty list, no node
 *         holding key found or no comparison function provided.
 */
int
slist_del(struct slist *list, void *key);

/**
 * @brief Deletes the first node in the list.
 *
 * @param list
 *   Pointer to the list.
 *
 * @return Pointer to the deleted node's data in case of success.
 *
 * @retval NULL in case of invalid pointer to the list or empty list.
 */
int
slist_del_front(struct slist *list);

/**
 * @brief Deletes the last node in the list.
 *
 * @param list
 *   Pointer to the list.
 *
 * @return Pointer to the deleted node's data in case of success.
 *
 * @retval NULL in case of invalid pointer to the list or empty list.
 */
int
slist_del_back(struct slist *list);

/**
 * @brief Finds the node in the list carrying key.
 *
 * @param list
 *   Pointer to the list.
 *
 * @return Pointer to the node's data in case of success.
 *
 * @retval NULL in case of invalid pointer to the list, empty list or no node
 *         holding key found.
 */
void *
slist_find(struct slist *list, void *key);

/**
 * @brief Calls on each list's node a user provided function.
 *
 * @param list
 * @param data
 * @param walk
 *
 * @return The error code of the function.
 *
 * @retval EOK     in case of success.
 * @retval -EINVAL in case of invalid pointer to the list data structure.
 * @retval -EUNSET in case user provided no walk function
 */
int
slist_walk(struct slist *list, void *data, walk_fn walk);

/**
 * @brief Prints the content of the list.
 *
 * @oaram list
 *   Pointer to the list.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the list or NULL pointer
 *         to print function
 * @retval EOK in case of success.
 */
int
slist_print(struct slist *list);

/**
 * @brief Uninitialises the list.
 *
 * @param list
 *   Pointer to the list.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the list.
 * @retval EOK in case of success.
 */
int
slist_deinit(struct slist *list);
#endif /* _SLIST_H */
