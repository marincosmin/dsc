#include <stdio.h>
#include <stdlib.h>

#include "graph.h"
#include "queue.h"

int main(int argc, char *args[])
{
    int v[] = {0, 1, 1, 2, 1, 3, 2, 4, 4, 6, 5, 6, 6, 3};
    //int v[] = {0, 1, 1, 2, 1, 3, 2, 4, 3, 5, 4, 6, 5, 6, 6, 3};
    //int v[] = {0, 1, 1, 2, 2, 3, 3, 0};
    struct graph *g;

    //graph_initv(&graph, UNIDIRECTIONAL, 7, NULL, 8, v, NULL); 
    graph_initv(&g, UNIDIRECTIONAL, 7, NULL, 7, v, NULL); 
    //graph_initv(&graph, UNIDIRECTIONAL, 4, NULL, 4, v, NULL); 
    
    graph_print(g);
    printf("has_cycle: %d\n", graph_has_cycle(g));

    graph_deinit(g);

    return 0;
}
