#include <stdlib.h>

#include "dlist.h"

#define INIT_DLIST_NODE(node, data)                                               \
    do {                                                                    \
        node = calloc(1, sizeof(struct dlist_node));                        \
        if (NULL == list)                                                   \
            return -ENOMEM;                                                 \
        node->data = (data);                                                \
    } while (0)

//TODO: not allocating the list allows reuse of the same data structure
int
dlist_init(struct dlist **list, walk_fn walk, print_fn print,
           deinit_fn deinit, compare_fn compare)
{
    struct dlist *dl = NULL;

    if (NULL == list)
        return -EINVAL;

    dl = calloc(1, sizeof(struct dlist));
    if (NULL == dl)
        return -ENOMEM;

    dl->head.next = &dl->tail;
    dl->tail.prev = &dl->head;

    dl->fntbl.walk = walk;
    dl->fntbl.print = print;
    dl->fntbl.deinit = deinit;
    dl->fntbl.compare = compare;

    *list = dl;

    return EOK;
}

inline int
dlist_add_front(struct dlist *list, void *data)
{
    struct dlist_node *head = NULL;
    struct dlist_node *node = NULL;
    struct dlist_node *succ = NULL;

    if (NULL == list)
        return -EINVAL;
   
    INIT_DLIST_NODE(node, data);

    head = DLIST_HEAD(list);

    succ = head->next;
    succ->prev = node;
    node->next = succ;
    node->prev = head;
    head->next = node;

    list->size++;

    return EOK;
}

inline int
dlist_add_back(struct dlist *list, void *data)
{
    struct dlist_node *tail = NULL;
    struct dlist_node *node = NULL;
    struct dlist_node *pred = NULL;

    if (NULL == list)
        return -EINVAL;

    INIT_DLIST_NODE(node, data);

    tail = DLIST_TAIL(list);

    pred = tail->prev;
    pred->next = node;
    node->prev = pred;
    node->next = tail;
    tail->prev = node;

    list->size++;

    return EOK;
}

inline int
dlist_insert(struct dlist *list, void *key, void *data)
{
    struct dlist_node *node = NULL;
    struct dlist_node *succ = NULL;
    struct dlist_node *iterator = NULL;

    if (NULL == list || NULL == key)
        return -EINVAL;

    iterator = DLIST_HEAD(list)->next;
    while (iterator != DLIST_TAIL(list)) {
        if (!list->fntbl.compare(iterator->data, key))
            break;
        iterator = iterator->next;
    }

    if (iterator == DLIST_TAIL(list))
        return -ENOTFND;
    
    INIT_DLIST_NODE(node, data);
    
    succ = iterator->next;
    iterator->next = node;
    node->prev = iterator;
    succ->prev = node;
    node->next = succ;

    list->size++;

    return EOK;
}

inline void *
dlist_del_front(struct dlist *list)
{
    void *data = NULL;
    struct dlist_node *head = NULL;
    struct dlist_node *succ = NULL;
    struct dlist_node *victim = NULL;

    if (NULL == list)
        return NULL;

    if (dlist_empty(list))
        return NULL;

    head = DLIST_HEAD(list);
    victim = head->next;
    succ = victim->next;
    head->next = succ;
    succ->prev = head;

    data = victim->data;

    free(victim);

    list->size--;

    return data;
}

// TODO: what should del return ? error code or pointer to the data
// of the removed element 

inline int
dlist_del(struct dlist *list, void *key)
{
    struct dlist_node *pred = NULL;
    struct dlist_node *succ = NULL;
    struct dlist_node *iter = NULL;

    if (NULL == list || NULL == key)
        return -EINVAL;

    iter = DLIST_HEAD(list)->next;
    while (iter != DLIST_TAIL(list)) {
        if (!list->fntbl.compare(iter->data, key))
            break;
        iter = iter->next;
    }

    if (iter == DLIST_TAIL(list))
        return -ENOTFND;
   
    pred = iter->prev;
    succ = iter->next;
    pred->next = succ;
    succ->prev = pred;

    if (list->fntbl.deinit)
        list->fntbl.deinit(iter->data, NULL);
    free(iter);

    list->size--;

    return EOK;
}

inline void *
dlist_del_back(struct dlist *list)
{
    void *data = NULL;
    struct dlist_node *tail = NULL;
    struct dlist_node *pred = NULL;
    struct dlist_node *victim = NULL;

    if (NULL == list)
        return NULL;
    
    if (dlist_empty(list))
        return NULL;

    tail = DLIST_TAIL(list);
    victim = tail->prev;
    pred = victim->prev;
    pred->next = tail;
    tail->prev = pred;

    data = victim->data;

    free(victim);

    list->size--;

    return data;
}

inline void *
dlist_find(struct dlist *list, void *key)
{
    struct dlist_node *iterator = NULL;

    if (NULL == list || NULL == key)
        return NULL;

    iterator = DLIST_HEAD(list)->next;
    while (iterator != DLIST_TAIL(list)) {
        if (!list->fntbl.compare(iterator->data, key))
            break;
        iterator = iterator->next;
    }

    if (iterator == DLIST_TAIL(list))
        return NULL;
    
    return iterator->data;
}

inline int
dlist_empty(struct dlist *list)
{
    if (NULL == list)
        return -EINVAL;

    return (list->size == 0);
}

inline int
dlist_size(struct dlist *list)
{
    if (NULL == list)
        return -EINVAL;

    return list->size;
}

int
dlist_walk(struct dlist *list, void *extra, walk_fn walk)
{
    int index = 0;
    walk_fn wfn = NULL;
    struct dlist_node *iter = NULL;

    if (NULL == list)
        return -EINVAL;
    
    // provided walk function takes precedence over the previously set one
    wfn = (NULL == walk) ? list->fntbl.walk : walk;
    if (NULL == wfn)
        return -EUNSET;

    iter = DLIST_HEAD(list)->next;
    while (iter != DLIST_TAIL(list)) {
        wfn(iter->data, &index, extra);
        iter = iter->next;
        index++;
    }

    return EOK;
}

inline int
dlist_print(struct dlist *list)
{
    int index = 0;
    struct dlist_node *iter = NULL;

    if (NULL == list)
        return -EINVAL;

    //TODO: find a better error code
    if (NULL == list->fntbl.print)
        return -EUNSET;

    iter = DLIST_HEAD(list)->next;
    while (iter != DLIST_TAIL(list)) {
        list->fntbl.print(iter->data, &index);
        iter = iter->next;
        index++;
    }

    return EOK;
}

int
dlist_deinit(struct dlist *list)
{
    struct dlist_node *iter = NULL;

    if (NULL == list)
        return -EINVAL;

    iter = DLIST_HEAD(list)->next;
    while (iter != DLIST_TAIL(list)) {
        struct dlist_node *tmp = iter->next;
        // perhaps there's nothing to deinit
        if (list->fntbl.deinit)
            list->fntbl.deinit(iter->data, NULL);
        free(iter);
        iter = tmp;
    }

    return EOK;
}
