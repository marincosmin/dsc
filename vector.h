#ifndef _VECTOR_H
#define _VECTOR_H

#include "common.h"

#define DEFAULT_CAPACITY    4

typedef enum {
    ASCENDING = 1,
    DESCENDING
} order;

struct vector {
    void **array;           /* array carrying the data */    
    unsigned size;          /* array's current size */
    unsigned capacity;      /* array's capacity */
    struct fn_table fntbl;  /* compare, deinit, print, walk functions*/
};

/**
 * @brief Initialises a vector.
 *
 * @param vector
 * @param capacity
 * @param walk
 * @param print
 * @param deinit
 * @param compare
 *
 * @return The error code of the function.
 *
 * @retval EOK     in case of success.
 * @retval -EINVAL in case of invalid pointer to a vector data structure.
 * @retval -ENOMEM in case of failure in allocating the memory to fulfill
 *                 vector's capacity.
 */
int
vector_init(struct vector *vector, const unsigned int capacity, walk_fn walk,
            print_fn print, deinit_fn deinit, compare_fn compare);

/**
 * @brief Dinitialises a vector.
 *
 * @param vector
 *
 * @return The error code of the function.
 *
 * @retval EOK     in case of success.
 * @retval -EINVAL in case of invalid pointer to a vector data structure.
 */
int
vector_deinit(struct vector *vector);

/**
 * @brief Adds a new element to the vector.
 *
 * @param vector
 *
 * @return The error code of the function.
 *
 * @retval EOK     in case of success.
 * @retval -EINVAL in case of invalid pointer to a vector data structure.
 */
int
vector_add(struct vector *vector, void *data);

/**
 * @brief Sets the value of the element at a particular index in the vector.
 *
 * @param vector
 * @param index
 * @param data
 *
 * @return The error code of the function.
 *
 * @retval EOK     in case of success.
 * @retval -EINVAL in case of invalid pointer to a vector data structure.
 * @retval -EUNINIT in case the vector is uninitialised, invalid size
 * @retval -EIDXOOB in case the index points out of vector's boundaries
 */
int
vector_set(struct vector *vector, unsigned index, void *data);

/**
 * @brief Inserts an element at a particular index in the vector.
 *
 * +shifts right
 *
 * @param vector
 * @param index
 * @param data
 *
 * @return The error code of the function.
 *
 * @retval EOK     in case of success.
 * @retval -EINVAL in case of invalid pointer to a vector data structure.
 * @retval -EUNINIT in case the vector is uninitialised, invalid size
 * @retval -EIDXOOB in case the index points out of vector's boundaries
 */
int
vector_insert(struct vector *vector, unsigned index, void *data);

/**
 * @brief Removes the element at a particular index in the vector.
 *
 * +shifts left
 *
 * @param vector
 * @param index
 *
 * @return The error code of the function.
 *
 * @retval EOK     in case of success.
 * @retval -EINVAL in case of invalid pointer to a vector data structure.
 * @retval -EUNINIT in case the vector is uninitialised, invalid size
 * @retval -EIDXOOB in case the index points out of vector's boundaries
 */
int
vector_del(struct vector *vector, unsigned index);

/**
 * @brief Returns the element at a particular index in the vector.
 *
 * @param vector
 * @param index
 *
 * @return Pointer to the element at index.
 *
 * @retval NULL in case of invalid pointer to vector, uninitialised vector or
 *              index out of bounds.
 */
void *
vector_get(struct vector *vector, unsigned index);

/**
 * @brief Returns the index of a particular element/data in the vector.
 *
 * @param vector
 * @param index
 * @param data
 *
 * @return The error code of the function.
 *
 * @retval EOK     in case of success.
 * @retval -EINVAL in case of invalid pointer to a vector data structure.
 * @retval -EUNINIT in case the vector is uninitialised, invalid size
 * @retval -EIDXOOB in case the index points out of vector's boundaries
 */
int
vector_index(struct vector *vector, unsigned index, void *data);

/**
 * @brief Returns the size of the vector.
 *
 * @param vector
 *
 * @return The size of the vector.
 *
 * @retval -EINVAL  in case of invalid pointer to a vector data structure.
 * @retval -EUNINIT in case the vector is uninitialised, invalid size
 */
int
vector_size(struct vector *vector);

/**
 * @brief Returns whether the vector is empty.
 *
 * @param vector
 *
 * @return
 *
 * @retval 1 (True).
 * @retval 0 (False).
 */
int
vector_empty(struct vector *vector);

/**
 * @brief Swaps the elements located at the provided indices in the vector.
 *
 * @param vector
 *   Pointer to the vector.
 * @param index1
 *   Index of the first element.
 * @param index2
 *   Index of the second element.
 *
 * @return The error code of the function.
 *
 * @retval EOK      in case of success.
 * @retval -EINVAL  in case of invalid pointer to a vector data structure.
 * @retval -EUNINIT in case the vector is uninitialised, invalid size
 * @retval -EIDXOOB in case the index points out of vector's boundaries
 */
int
vector_swap(struct vector *vector, unsigned index1, unsigned index2);

/**
 * @brief Compares the elements located at the provided indices in the vector.
 *
 * @param vector
 *
 * @return The error code of the function.
 *
 * @retval 0       in case of equality.
 * @retval +1      in case the first element is greater than the second.
 * @retval -1      in case the first element is smaller than the second.
 * @retval -EINVAL  in case of invalid pointer to a vector data structure.
 * @retval -EUNINIT in case the vector is uninitialised, invalid size
 * @retval -EIDXOOB in case the index points out of vector's boundaries
 *
 * @note The return values hold assuming the user provided compare function
 *       follows the convention.
 */
int
vector_compare(struct vector *vector, unsigned index1, unsigned index2);

/**
 * @brief Calls on each vector's element a user provided function.
 *
 * @param vector
 * @param data
 * @param walk
 *
 * @return The error code of the function.
 *
 * @retval EOK     in case of success.
 * @retval -EINVAL in case of invalid pointer to a vector data structure.
 * @retval -EUNSET in case user provided no walk function
 */
int
vector_walk(struct vector *vector, void *data, walk_fn walk);

/**
 * @brief Sorts the vector based on the user provided comparison function.
 *
 * @param vector
 * @param ord
 *
 * @return The error code of the function.
 *
 * @retval EOK     in case of success.
 * @retval -EINVAL in case of invalid pointer to a vector data structure.
 * @retval -EUNSET in case user provided no comparison function
 */
int
vector_sort(struct vector *vector, order ord);

/**
 * @brief Dumps the content of the array.
 *
 * @param vector
 * @param ord
 *
 * @return The error code of the function.
 *
 * @retval EOK     in case of success.
 * @retval -EINVAL in case of invalid pointer to a vector data structure.
 * @retval -EUNINIT in case the vector is uninitialised, invalid size
 */
int
vector_print(struct vector *vector);
#endif /*_VECTOR_H*/
