#ifndef QUEUE_H_
#define QUEUE_H_

#include "dlist.h"

struct queue {
    struct dlist dlist;
};

#define QUEUE_INIT(_queue, _walk, _print, _deinit, _compare)                \
    {                                                                       \
        .dlist = DLIST_INIT(_queue.dlist, _walk, _print, _deinit, _compare) \
    }                                                                       \

#define QUEUE(_queue, _walk, _print, _deinit, _compare)                     \
    struct queue _queue = QUEUE_INIT(_queue, _walk, _print, _deinit, _compare)

#define QUEUE_HEAD(_queue)      DLIST_HEAD(_queue.dlist)
#define QUEUE_TAIL(queue)       DLIST_TAIL(_queue.dlist)

/**
 * @brief Initialises the queue.
 *
 * @param[in]  queue
 *   Pointer to the queue.
 * @param walk
 * @param print
 *   Pointer to a function dumping the content of the node
 * @param deinit
 *   Pointer to a function handling deinitialisation of a node; can be NULL
 *   in case node deinitialisation isn't required.
 * @param compare
 *   Pointer to a comparison function that provides order relation between 
 *   list nodes; can be NULL in case list usage does not imply nodes
 *   comparison.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid pointer to the list.
 * @retval -ENOMEM in case memory allocation for the queue fails.
 * @retval EOK in case of success.
 */
int
queue_init(struct queue **queue, walk_fn walk, print_fn print,
           deinit_fn deinit, compare_fn compare);

/**
 * @brief Check whether the queue is empty.
 *
 * @param[in]  queue
 *   Pointer to the queue.
 *
 * @return
 *
 * @retval 1 (True).
 * @retval 0 (False).
 */
int
queue_empty(struct queue *queue);

/**
 * @brief Get the size of a list.
 *
 * @param[in]  queue
 *   Pointer to the queue.
 *
 * @return The number of elements in the list.
 *
 * @retval EINVAL for invalid parameter. 
 * @retval Integer number >= 0.
 */
int
queue_size(struct queue *queue);

/**
 * @brief Adds an element to the queue.
 *
 * @param[in]  queue
 *   Pointer to the queue.
 * @param[in]  data
 *   The data to be carried by the new node to be added.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid pointer to the list.
 * @retval -ENOMEM if a new node can't be allocated.
 * @retval EOK for success.
 * 
 */
int
queue_enqueue(struct queue *queue, void *data);

/**
 * @brief Remove an element from the queue.
 *
 * @param[in]  queue
 *   Pointer to the queue.
 *
 * @return Pointer to the victim node.
 *
 * @retval NULL in case of empty list or invalid parameter.
 */
void *
queue_dequeue(struct queue *queue);

/**
 * @brief Calls on each queue's node a user provided function.
 *
 * @param list
 * @param data
 * @param walk
 *
 * @return The error code of the function.
 *
 * @retval EOK     in case of success.
 * @retval -EINVAL in case of invalid pointer to the queue data structure.
 * @retval -EUNSET in case user provided no walk function
 */
int
queue_walk(struct queue *queue, void *data, walk_fn walk);

/**
 * @brief Uninitialises the queue.
 *
 * @param queue
 *   Pointer to the queue.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the queue.
 * @retval EOK in case of success.
 */
int
queue_deinit(struct queue *queue);

/**
 * @brief Prints the content of the queue.
 *
 * @oaram list
 *   Pointer to the list.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL in case of invalid pointer to the queue or unset print
 *         function.
 * @retval EOK in case of success.
 */
int
queue_print(struct queue *queue);

/**
 *
 */
int
queue_contains(struct queue *queue, void *data);
#endif /* QUEUE_H_*/
