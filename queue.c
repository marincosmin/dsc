#include <stdlib.h>
#include <string.h>

#include "queue.h"

int
queue_init(struct queue **queue, walk_fn walk, print_fn print,
           deinit_fn deinit, compare_fn compare)
{
    struct queue *q = NULL;

    if (NULL == queue)
        return -EINVAL;

    q = calloc(1, sizeof(struct queue));
    if (NULL == q)
        return -ENOMEM;

    struct dlist d = DLIST_INIT(q->dlist, walk, print, deinit, compare);
    memcpy(&q->dlist, &d, sizeof(struct dlist));

    *queue = q;

    return EOK;
}

int
queue_deinit(struct queue *queue)
{
    if (NULL == queue)
        return -EINVAL;

    return dlist_deinit(&queue->dlist);
}

int
queue_empty(struct queue *queue)
{
    if (queue == NULL)
        return -EINVAL;

    return dlist_empty(&queue->dlist);
}

int
queue_size(struct queue *queue)
{
    if (queue == NULL)
        return -EINVAL;

    return dlist_size(&queue->dlist);
}

int
queue_enqueue(struct queue *queue, void *data)
{
    if (NULL == queue)
        return -EINVAL;

    return dlist_add_front(&queue->dlist, data);
}

void *
queue_dequeue(struct queue *queue)
{
    if (NULL == queue)
        return NULL;

    return dlist_del_back(&queue->dlist);
}

int
queue_walk(struct queue *queue, void *data, walk_fn walk)
{
    if (NULL == queue)
        return - EINVAL;

    return dlist_walk(&queue->dlist, data, walk);
}

int
queue_print(struct queue *queue)
{
    if (NULL == queue)
        return -EINVAL;

    return dlist_print(&queue->dlist);
}
