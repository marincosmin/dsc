#ifndef _GRAPH_H
#define _GRAPH_H

#include "slist.h"

#define MAX_DISTANCE    (9223372036854775807L)

enum color {
    WHITE = 1,  // unknown
    GRAY  = 2,  // discovered
    BLACK = 3   // explored / visited
};

enum flags {
    WEIGHTED = 0x01,
    DIRECTED = 0x02,
};

#define IS_WEIGHTED_GRAPH(flags)                                            \
    ((flags) & WEIGHTED)
#define IS_DIRECTED_GRAPH(flags)                                            \
    ((flags) & DIRECTED)

#define IS_WEIGHTED_DIRECTED_GRAPH(flags)                                   \
    (IS_WEIGHTED_GRAPH(flags) && IS_DIRECTED_GRAPH(flags))
#define IS_WEIGHTED_UNDIRECTED_GRAPH(flags)                                 \
    (IS_WEIGHTED_GRAPH(flags) && !IS_DIRECTED_GRAPH(flags))
#define IS_UNWEIGHTED_DIRECTED_GRAPH(flags)                                 \
    (!IS_WEIGHTED_GRAPH(flags) && IS_DIRECTED_GRAPH(flags))
#define IS_UNWEIGHTED_UNDIRECTED_GRAPH(flags)                               \
    (!IS_WEIGHTED_GRAPH(flags) && !IS_DIRECTED_GRAPH(flags))

struct graph_node;

struct edge {
    int weight;
    struct graph_node *neighbour;
};

struct graph_node {
    int               label;        // node label
    long              distance;     // the distance from previous node
    enum color        color;        // node state expressed as color
    struct slist      *edges;       // list of edges
    struct graph_node *previous;    // pointer to previous node
};

struct graph {
    unsigned          num_nodes;    // number of nodes in the graph
    unsigned          num_edges;    // number of edges in the graph
    struct graph_node *nodes;       // list of nodes in the graph
};

#define for_each_node(_graph, _node)                                        \
    for (int n = 0;                                                         \
         (n < (_graph)->num_nodes) && ((_node) = &(_graph)->nodes[n], 1);   \
         n++)

#define for_each_edge(_node, _edge)                                         \
    if ((NULL != (_node)) && (NULL != (_node)->edges))                      \
       slist_for_each((_node)->edges, struct edge, _edge)

/**
 * @brief Initialises a graph based on a list of edges.
 *
 * @param graph
 *   Pointer to the graph data structure.
 * @param type
 *   The type of graph: WEIGHTED | BIDIRECTIONAL | UNIDIRECTIONAL.
 * @param num_nodes
 *   The number of nodes composing the actual graph.
 * @param labels
 *   Pointer to an array of node labels.
 * @param num_edges
 *   The number of edges.
 * @param ...
 *   num_edges pairs of node labels, each pair describing a graph edge
 *   (a pair is described by two consecutive node labels and implies there is
 *   a way from the leftside node to the rightside one).
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid pointer to the heap.
 * @retval -ENOMEM in case heap memory allocation fails.
 * @retval EOK in case of success.
 */
int
graph_initl(struct graph **graph, int flags, unsigned num_nodes,
            int *labels, unsigned num_edges, ...);

/**
 * @brief Initialises a graph based on an array of edges.
 *
 * @param graph
 *   Pointer to the graph data structure.
 * @param type
 *   The type of graph: WEIGHTED | BIDIRECTIONAL | UNIDIRECTIONAL.
 * @param num_nodes
 *   The number of nodes composing the actual graph.
 * @param labels
 *   Pointer to an array of node labels.
 * @param num_edges
 *   The number of edges.
 * @param edges
 *   Pointer to an array contining pairs of node labels. Each pair describes
 *   an edge in the graph (a pair is described by two consecutive node labels
 *   and implies there is a way from the leftside node to the rightside one).
 * @param weights
 *   Pointer to an array of weigths associated to the array of edges.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid graph pointer..
 * @retval -ENOMEM in case heap memory allocation fails.
 * @retval EOK in case of success.
 */
int
graph_initv(struct graph **graph, int flags, unsigned  num_nodes,
            int *labels, unsigned num_edges, int *edges, int *weights);

/**
 * @brief Initialises a graph based on a file.
 *
 * @param file
 *   The file to read the graph from.
 * @param graph
 *   Pointer to the graph data structure.
 * @param type
 *   The type of graph: WEIGHTED | BIDIRECTIONAL | UNIDIRECTIONAL.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid graph pointer.
 * @retval -ENOMEM in case heap memory allocation fails.
 * @retval EOK in case of success.
 */
int
graph_initf(char *file, struct graph **graph, int flags);

/**
 * @brief Returns a pointer to the graph node.
 *
 * @param graph
 *   Pointer to the graph data structure.
 *
 * @return Pointer to the node or NULL if it doesn't exist.
 *
 * @retval NULL for invalid graph pointer or the node doesn't exist..
 * @retval pointer to node.
 */
struct graph_node *
graph_get_node(struct graph *graph, int label);

/**
 * @brief Returns the number of nodes in the graph.
 *
 * @param graph
 *   Pointer to the graph data structure.
 *
 * @return Number of nodes.
 *
 * @retval -EINVAL for invalid graph pointer.
 * @retval >= 0, number of nodes in the graph.
 */
int
graph_get_num_nodes(struct graph *graph);

/**
 * @brief Returns the number of edges in the graph.
 *
 * @param graph
 *   Pointer to the graph data structure.
 *
 * @return Number of odges.
 *
 * @retval -EINVAL for invalid graph pointer.
 * @retval >= 0, number of edges in the graph.
 */
int
graph_get_num_edges(struct graph *graph);

/**
 * @brief Checks whether a graph contains cycles.
 *
 * @param graph
 *   Pointer to the graph data structure.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid graph pointer.
 * @retval 0 in case no cycle(s) is/are found.
 * @retval 1 in case cycle(s) is/are found.
 */
int
graph_has_cycle(struct graph *graph);

/**
 * @brief Prints the graph.
 *
 * @param graph
 *   Pointer to the graph data structure.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid graph pointer.
 * @retval EOK in case of success.
 */
int
graph_print(struct graph *graph);

/**
 * @brief Deinitialises the graph.
 *
 * @param graph
 *   Pointer to the graph data structure.
 *
 * @return The error code of the function.
 *
 * @retval -EINVAL for invalid graph pointer.
 * @retval EOK in case of success.
 */
int
graph_deinit(struct graph *graph);
#endif
