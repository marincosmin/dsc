#include <stdint.h>
#include <stddef.h>

/*
 * This takes 10n+3 instructions. The size of tab[][256] is the maximum number
 * of input bytes. Values of tab[][256] are chosen at random. This can implement
 * universal hashing, but is more general than universal hashing.
 *
 * Zobrist hashes are especially favored for chess, checkers, othello, and other
 * situations where you have the hash for one state and you want to compute the
 * hash for a closely related state. You xor to the old hash the table values
 * that you're removing from the state, then xor the table values that you're
 * adding. For chess, for example, that's 2 xors to get the hash for the next
 * position given the hash of the current position.
 */
#if 0
ub4 zobrist_hash(char *key, ub4 len, ub4 mask, ub4 tab[MAXBYTES][256])
{
    ub4 hash, i;
    for (hash=len, i=0; i<len; ++i)
        hash ^= tab[i][key[i]];
    return (hash & mask);
}
#endif
//#if 0

#if !defined (get16bits)
#define get16bits(d) ((((uint32_t)(((const uint8_t *)(d))[1])) << 8)\
                       +(uint32_t)(((const uint8_t *)(d))[0]) )
#endif

uint32_t
super_fast_hash(const uint8_t* data, uint32_t length, uint32_t *seeds)
{
    uint32_t hash = length, tmp;
    int rem;

    if (data == NULL || seeds == NULL)
        return 0;

    rem = length & 3;
    length >>= 2;

    for ( ; length > 0; length--) {
        hash  += get16bits (data);
        tmp    = (get16bits (data + 2) << 11) ^ hash;
        hash   = (hash << 16) ^ tmp;
        data  += 2 * sizeof (uint16_t);
        hash  += hash >> 11;
    }

    // Handle end cases
    switch (rem) {
        case 3:
            hash += get16bits (data);
            hash ^= hash << 16;
            hash ^= ((signed char)data[sizeof (uint16_t)]) << 18;
            hash += hash >> 11;
            break;
        case 2:
            hash += get16bits (data);
            hash ^= hash << 11;
            hash += hash >> 17;
            break;
        case 1:
            hash += (signed char)*data;
            hash ^= hash << 10;
            hash += hash >> 1;
    }

    // Force "avalanching" of final 127 bits
    hash ^= hash << 3;
    hash += hash >> 5;
    hash ^= hash << 4;
    hash += hash >> 17;
    hash ^= hash << 25;
    hash += hash >> 6;

    return hash;
}
//#endif
